package tw.teddysoft.common.annotation;
/**
 * @author Teddy Chen
 */

import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * a hyperlink of source code.
 */
@Target(value={TYPE,METHOD,CONSTRUCTOR})
@Retention(value=RetentionPolicy.SOURCE)
public @interface See {
	@SuppressWarnings("unchecked")
	Class [] value(); 
	String type() default "";  
}
