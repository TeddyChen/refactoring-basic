package tw.teddysoft.kata.tennis.take4.ans;

import java.util.Arrays;
import java.util.List;

public class TennisGame4 implements TennisGame {

    static List<String> SCORE_MAP = Arrays.asList("Love","Fifteen","Thirty", "Forty");

    int player1Score = 0;
    int player2Score = 0;
    String player1Name;
    String player2Name;

    private GameState state;

    public TennisGame4(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;

        state = new Initial(this);
    }

    void changeState(GameState newState){
        state = newState;
    }


    @Override
    public void wonPoint(String playerName) {
        if (playerName.equals(this.player1Name))
            player1Score++;
        else if (playerName.equals(this.player2Name))
            player2Score++;
        else
            throw new RuntimeException("Cannot find player name: " + playerName);

        nextState();
    }

    private void nextState(){
        state.nextState();
    }


    @Override
    public String getScore() {
        return state.getScore();
    }

    boolean isOneOfPlayerOverForty() {
        return player1Score >= 4 || player2Score >=4;
    }

    boolean isTie() {
        return player1Score == player2Score;
    }

    boolean isDeuce() {
        return isTie() && player1Score >= 3;
    }

    boolean isGameOver() {
        return  (isOneOfPlayerOverForty() && Math.abs(player1Score - player2Score) >= 2);
    }

    boolean isAdvantage() {
        return  (isOneOfPlayerOverForty() && Math.abs(player1Score - player2Score) == 1);
    }

}