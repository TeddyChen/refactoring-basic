package tw.teddysoft.kata.tennis.take5.ans;

public class Initial extends GameState {

    public Initial(TennisGame5 game) {
        super(game);
    }

    @Override
    public String getScore() {
        if(game.isTie())
            return game.SCORE_MAP[game.player1Score] + "-All";
        else{
            return game.SCORE_MAP[game.player1Score] + "-" +
                    game.SCORE_MAP[game.player2Score];
        }
    }
}
