package tw.teddysoft.kata.tennis.take5.ans;

public abstract class GameState {

    protected TennisGame5 game;

    public GameState(TennisGame5 game) {
        this.game = game;
    }

    abstract String getScore();

}
