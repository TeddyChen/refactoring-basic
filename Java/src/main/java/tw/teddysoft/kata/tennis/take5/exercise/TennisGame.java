package tw.teddysoft.kata.tennis.take5.exercise;

public interface TennisGame {
    void wonPoint(String playerName);
    String getScore();
}