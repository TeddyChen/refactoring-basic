package tw.teddysoft.kata.tennis.take4.ans;

public interface TennisGame {
    void wonPoint(String playerName);
    String getScore();
}