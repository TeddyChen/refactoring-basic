package tw.teddysoft.kata.tennis.take2.ans;

public class TennisGame2 implements TennisGame
{
    private String[] SCORE_MAP = new String[]{"Love", "Fifteen", "Thirty", "Forty"};
    public int player1Score = 0;
    public int player2Score = 0;

    private String player1Name;
    private String player2Name;

    public TennisGame2(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public String getScore(){

        if (isTieButNotDeuce())
        {
            return SCORE_MAP[player1Score] + "-All";
        }

        if (isDeuce())
            return "Deuce";

        if (isNotTieBeforeDeuce()){
            return SCORE_MAP[player1Score] + "-" + SCORE_MAP[player2Score];
        }

        if (isAdvantage())
        {
            return "Advantage " + getLeadingPlayerName();
        }

        if (isGameOver())
        {
            return "Win for " + getLeadingPlayerName();
        }

        throw new RuntimeException("Infeasible path.");
    }

    private String getLeadingPlayerName(){
        return player1Score > player2Score ? player1Name : player2Name;
    }

    private boolean isGameOver() {
        return Math.abs(player1Score - player2Score) >= 2 && ((player1Score >= 4) || (player2Score >= 4));
    }

    private boolean isAdvantage() {
        return Math.abs(player1Score - player2Score) == 1 && player1Score >= 3 && player2Score >= 3;
    }

    private boolean isNotTieBeforeDeuce(){
        return (player1Score != player2Score) && (player1Score <= 3) && (player2Score <= 3);
    }

    private boolean isDeuce() {
        return player1Score == player2Score && player1Score >=3;
    }

    private boolean isTieButNotDeuce() {
        return player1Score == player2Score && player1Score <= 2;
    }

    public void wonPoint(String playerName) {
        if (playerName.equals(this.player1Name))
            player1Score++;
        else if (playerName.equals(this.player2Name))
            player2Score++;
        else
            throw new RuntimeException("Cannot find player name: " + playerName);
    }
}