package tw.teddysoft.kata.tennis.take5.ans;



public class Deuce extends GameState {

    public Deuce(TennisGame5 game) {
        super(game);
    }


    @Override
    public String getScore() {
        return "Deuce";
    }


}
