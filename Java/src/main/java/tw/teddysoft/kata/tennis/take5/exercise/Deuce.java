package tw.teddysoft.kata.tennis.take5.exercise;

public class Deuce extends GameState {

    public Deuce(TennisGame5 game) {
        super(game);
    }


    @Override
    public String getScore() {
        return "Deuce";
    }

    @Override
    public void nextState() {

        if (!game.isDeuce())
            game.changeState(new Advantage(game));

    }
}
