package tw.teddysoft.kata.tennis.take5.exercise;

public class Initial extends GameState {

    public Initial(TennisGame5 game) {
        super(game);
    }

    @Override
    public String getScore() {
        if(game.isTie())
            return game.SCORE_MAP.get(game.player1Score) + "-All";
        else{
            return TennisGame5.SCORE_MAP.get(game.player1Score) + "-" +
                    TennisGame5.SCORE_MAP.get(game.player2Score);
        }
    }

    @Override
    public void nextState() {
        if (game.isDeuce())
            game.changeState(new Deuce(game));
        else if (game.isGameOver())
            game.changeState(new GameOver(game));

    }
}
