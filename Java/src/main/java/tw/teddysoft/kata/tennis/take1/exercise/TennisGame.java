package tw.teddysoft.kata.tennis.take1.exercise;

public interface TennisGame {
    void wonPoint(String playerName);
    String getScore();
}