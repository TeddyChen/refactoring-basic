package tw.teddysoft.kata.tennis.take1.ans;

public interface TennisGame {
    void wonPoint(String playerName);
    String getScore();
}