package tw.teddysoft.kata.tennis.take2.ans;

public interface TennisGame {
    void wonPoint(String playerName);
    String getScore();
}