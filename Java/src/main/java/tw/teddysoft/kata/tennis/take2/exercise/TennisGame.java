package tw.teddysoft.kata.tennis.take2.exercise;

public interface TennisGame {
    void wonPoint(String playerName);
    String getScore();
}