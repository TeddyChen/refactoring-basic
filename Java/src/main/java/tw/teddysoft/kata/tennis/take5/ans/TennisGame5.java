package tw.teddysoft.kata.tennis.take5.ans;


public class TennisGame5 implements TennisGame {

    String[] SCORE_MAP = new String[]{"Love", "Fifteen", "Thirty", "Forty"};
    int player1Score = 0;
    int player2Score = 0;
    String player1Name;
    String player2Name;
    private GameState state;
    GameState INITIAL = new Initial(this);
    GameState DEUCE = new Deuce(this);
    GameState ADVANTAGE = new Advantage(this);
    GameState GAMEOVER = new GameOver(this);

    public TennisGame5(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
        state = INITIAL;
    }

    public String getScore() {
        return state.getScore();
    }

    @Override
    public void wonPoint(String playerName) {
        if (playerName.equals(this.player1Name))
            player1Score++;
        else if (playerName.equals(this.player2Name))
            player2Score++;
        else
            throw new RuntimeException("Cannot find player name: " + playerName);
        nextState();
    }

    private void nextState(){
        if (isDeuce())
            state = DEUCE;
        else if (isGameOver())
            state = GAMEOVER;
        else if (isAdvantage())
            state = ADVANTAGE;
    }

    boolean isReadyToWin() {
        return player1Score >= 4 || player2Score >=4;
    }

    boolean isTie() {
        return player1Score == player2Score;
    }

    boolean isDeuce() {
        return isTie() && player1Score >= 3;
    }

    boolean isGameOver() {
        return  (isReadyToWin() && Math.abs(player1Score - player2Score) >= 2);
    }

    boolean isAdvantage() {
        return  (isReadyToWin() && Math.abs(player1Score - player2Score) == 1);
    }
}