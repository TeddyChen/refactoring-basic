package tw.teddysoft.kata.tennis.take1.ans;

public class TennisGame1 implements TennisGame {
    private String[] SCORE_MAP = new String[]{"Love", "Fifteen", "Thirty", "Forty"};
    private int player1Score = 0;
    private int player2Score = 0;
    private String player1Name;
    private String player2Name;

    public TennisGame1(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public void wonPoint(String playerName) {
        if (playerName.equals(this.player1Name))
            player1Score++;
        else if (playerName.equals(this.player2Name))
            player2Score++;
        else
            throw new RuntimeException("Cannot find player name: " + playerName);
    }

    public String getScore() {
        if (isTie()) {
            return getTieScore();
        }
        else if (isAdvantage()) {
            return getAdvantageScore();
        }
        else if (isGameOver()) {
            return getGameOverScore();
        }
        else  {
            return getNormalScore();
        }
    }

    private String getNormalScore() {
        return SCORE_MAP[player1Score] + "-" + SCORE_MAP[player2Score];
    }

    private boolean anyPlayerGetFourPoints() {
        return player1Score >= 4 || player2Score >= 4;
    }

    private boolean isAdvantage(){
        return anyPlayerGetFourPoints() && Math.abs(player1Score-player2Score) == 1;
    }
    private String getAdvantageScore(){
        return "Advantage " + getLeadingPlayerName();
    }

    private boolean isGameOver(){
        return anyPlayerGetFourPoints() && Math.abs(player1Score-player2Score) >= 2;
    }

    private String getGameOverScore(){
        return "Win for " + getLeadingPlayerName();
    }

    private String getLeadingPlayerName(){
        return player1Score > player2Score ? player1Name : player2Name;
    }


    private String getTieScore() {
        if (player1Score >= 3){
            return "Deuce";
        }
        else
            return SCORE_MAP[player1Score] + "-All";
    }

    private boolean isTie() {
        return player1Score == player2Score;
    }
}