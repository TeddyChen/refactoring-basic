package tw.teddysoft.kata.tennis.take3.exercise;

public interface TennisGame {
    void wonPoint(String playerName);
    String getScore();
}