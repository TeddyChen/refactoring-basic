package tw.teddysoft.kata.tennis.take4.ans;

public class Deuce extends GameState {

    public Deuce(TennisGame4 game) {
        super(game);
    }


    @Override
    public String getScore() {
        return "Deuce";
    }

    @Override
    public void nextState() {

        if (!game.isDeuce())
            game.changeState(new Advantage(game));

    }
}
