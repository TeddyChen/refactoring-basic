package tw.teddysoft.kata.gildedrose.take3.ans;

public class Sulfuras extends Item {

    public Sulfuras(int sellIn, int quality) {

        super("Sulfuras, Hand of Ragnaros", sellIn, quality);
    }

    @Override
    protected void doUpdateQuality() {
    }
}
