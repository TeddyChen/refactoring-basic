
This example was introduced by Martin Fowler in this book Refactoring: Improving the Design of Existing Code.
The original source code is downloaded from https://github.com/tobyweston/Refactoring-Chapter-1 to save typing time.

In practicing phase 1 and phase 6, you can use the following linux command to approve the test results.

# phase 1
mv CustomerTest.testStatement.received.txt CustomerTest.testStatement.approved.txt

#phase 6
mv CustomerTest.testHtmlStatement.received.txt CustomerTest.testHtmlStatement.approved.txt