package tw.teddysoft.chapter1.ans.phase8;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<Rental>();

    public Customer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addRental(Rental rental) {
        rentals.add(rental);
    }


    public String statement() {
        String result = "Rental record for " + getName() + "\n";

        for (Rental each : rentals) {
            // show figures for this rental
            result += "\t" + each.getMovie().getTitle() + "\t" + String.valueOf(each.getCharge()) + "\n";
        }

        result += "Amount owed is " + String.valueOf(getTotalCharge()) + "\n";
        result += "You earned " + String.valueOf(getTotalFrequentRenterPoints()) + " frequent renter points";

        return result;
    }

    public String htmlStatement() {
        String result = "<H1>Rental record for <EM>" + getName() + "</EM></H1><P>\n";

        for (Rental each : rentals) {
            result += each.getMovie().getTitle() + ": " + String.valueOf(each.getCharge()) + "<BR>\n";
        }

        result += "<P>Amount owed is <EM>" + String.valueOf(getTotalCharge()) + "</EM>\n";
        result += "You earned <EM>" + String.valueOf(getTotalFrequentRenterPoints()) + " </EM> frequent renter points <P>";

        return result;
    }

    private int getTotalFrequentRenterPoints(){
        int result = 0;
        for (Rental each : rentals) {
            result += each.getFrequentRenterPoints();
        }
        return result;
    }

    private double getTotalCharge(){
        double result = 0;
        for (Rental each : rentals) {
            result += each.getCharge();
        }
        return result;
    }

}
