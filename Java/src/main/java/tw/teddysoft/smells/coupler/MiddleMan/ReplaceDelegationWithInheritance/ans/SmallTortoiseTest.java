/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.MiddleMan.ReplaceDelegationWithInheritance.ans;

import static org.junit.Assert.*;

import org.junit.Test;

public class SmallTortoiseTest {

	@Test
	public void when_SmallTortoise_replaces_delegation_with_inheritance_then_the_middle_man_is_removed() {
		Hub st = new SmallTortoise();
		assertTrue(st.wireConnect());
	}
}
