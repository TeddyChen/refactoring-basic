/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.MoveMethod.exercise;

import java.util.Hashtable;
import java.util.Map;

public class StoragePool {
	private static final int PERFORMANCE_FACTOR = 40;
	private Hashtable<String, Storage> pools = new Hashtable<>();
	
	public StoragePool(){
	}
	
	public void addStorage(Storage storage){
		pools.put(storage.geName(), storage);
	}
	
	public String compress(String storageName){
		if (getStorage(storageName).getUsagePercentage() < PERFORMANCE_FACTOR)
			return fastCompress(getStorage(storageName));
		else
			return standardCompress(getStorage(storageName));
	}
	
	public void compressAll(){
		for (Map.Entry<String, Storage> entry : pools.entrySet())
		{
			compress(entry.getKey());
		}
	}
	
	private Storage getStorage(String name){
		return pools.get(name);
	}
	
	private String fastCompress(Storage arg){
		// implementation
		return "fast compress";
	}
	
	private String standardCompress(Storage arg){
		// implementation
		return "standard compress";
	}
	
	// a lot of code here
}
