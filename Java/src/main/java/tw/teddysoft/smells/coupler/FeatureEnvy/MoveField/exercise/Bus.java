/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.FeatureEnvy.MoveField.exercise;

public class Bus {
	private Driver _driver;
	private int _ticketPrice;
	
	public Bus(int ticketPrice){
		_ticketPrice = ticketPrice;
	}

	public void setDriver(Driver arg){
		_driver = arg;
	}
	
	public int ticketPrice(){
		int result =  _ticketPrice;
		if (_driver.getBusType() == BusType.SPECIAL){
			result = result * 2;
		}
		else if (_driver.getBusType() == BusType.FREE){
			result = 0;
		}
		return result;
	}

	public int serivceIntervalInMinutes(){
		int result = -1;
		if (_driver.getBusType() == BusType.SPECIAL){
			result = 5;
		}
		else if (_driver.getBusType() == BusType.FREE){
			result = 30;
		}
		else if (_driver.getBusType() == BusType.REGULAR){
			result = 10;
		}
		return result;
	}
}
