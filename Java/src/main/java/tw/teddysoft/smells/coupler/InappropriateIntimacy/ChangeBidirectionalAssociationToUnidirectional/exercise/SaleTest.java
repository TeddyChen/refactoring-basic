/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.ChangeBidirectionalAssociationToUnidirectional.exercise;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class SaleTest {
	Item milk;
	Item cookie;
	SalesLineItem lineItem1;
	SalesLineItem lineItem2;
	
	@Before
	public void setUp(){
		milk = new Item("Milk", 80);
		cookie = new Item("Cookie", 25);
		lineItem1 = new SalesLineItem(milk, 1);
		lineItem2 = new SalesLineItem(cookie, 3);
	}
	
	@Test
	public void when_add_4_items_by_2_SalesLineItem_in_a_sale_then_getNumberOfItems_is_4() {
		Sale sale = new Sale("SN0089711");
		sale.addLineItem(lineItem1);
		sale.addLineItem(lineItem2);
		assertEquals(4, sale.getNumberOfItems());
	}

	@Test
	public void when_add_a_lineItem_into_a_sale_then_can_get_the_sale_from_the_lineItem() {
		Sale sale = new Sale("SS001");
		sale.addLineItem(lineItem1);
		assertEquals("SS001", lineItem1.getSale().getID());
	}

}
