/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.ChangeBidirectionalAssociationToUnidirectional.ans;

public class SalesLineItem {
	private Item _item;
	private int _quantity;
//	private Sale _sale;
	
	public SalesLineItem(Item item, int quantity){
		_item = item;
		_quantity = quantity;
	}
	
//	public void setSale(Sale arg){
//		_sale = arg;
//	}
//	
//	public Sale getSale(){
//		return _sale;
//	}
	
	public int getQuantity(){
		return _quantity;
	}
	
	public Item getItem(){
		return _item;
	}
}
