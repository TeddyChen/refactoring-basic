/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.MoveMethod.exercise;

import java.util.LinkedList;
import java.util.List;

public class Storage {
	private static final int PERFORMANCE_FACTOR = 40;
	private final String _name;
	private final int _maxNumberOfItem;
	private List<Object> _space;
	
	public Storage(String name, int maxNumberOfItem){
		_name = name;
		_maxNumberOfItem = maxNumberOfItem;
		_space = new LinkedList<>();
	}
	
	public String geName(){
		return _name;
	}
	
	public int getUsagePercentage(){
		return (int)(((double)_space.size()) / _maxNumberOfItem * 100);
	}
	
	public String save(Object arg){
		if (getUsagePercentage() < PERFORMANCE_FACTOR)
			return fastSave(arg);
		else
			return standardSave(arg);
	}
	
	public void delete(Object arg){
		_space.remove(arg);
	}
	
	public void defragment(){
	}
	
	private String fastSave(Object arg){
		_space.add(arg);
		return "fast save";
	}
	private String standardSave(Object arg){
		_space.add(arg);
		return "standard save";
	}
	
}
