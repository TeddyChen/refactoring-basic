/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.ReplaceInheritanceWithDelegation.ans;

public class Engine307 {
	private boolean _isInternalTesting = false;
	private boolean _underMonitoring;
	private int _speed;
	private boolean _manualControl;
	
	public int run(){
		if (_underMonitoring)
			startDataRecording();
		
		if(_isInternalTesting)
			return TestRun();
		if(_manualControl)
			return manualControlRun();
		
		return normalRun();
	}
	
	public void setUnderMonitoring(boolean underMonitoring){
		_underMonitoring = true;
	}
	
	public boolean isUnderMonitoring(){
		return _underMonitoring;
	}
	
	public boolean isManualControl(){
		return _manualControl;
	}
	
	public void setManualControl(int speed){
		_manualControl = true;
		_speed = speed;
	}
	
	public void startDataRecording(){
		// a lot of code
	}
	
	protected int TestRun(){
		// a lot of code
		_speed = 1000;
		return _speed;
	}

	protected int manualControlRun(){
		// a lot of code
		return _speed;
	}
	
	protected int normalRun(){
		// a lot of code
		_speed = 5000;
		return _speed;
	}

	protected void setTestMode(){
		_isInternalTesting = true;
	}
	
	protected void setNormalMode(){
		_isInternalTesting = false;
	}
}
