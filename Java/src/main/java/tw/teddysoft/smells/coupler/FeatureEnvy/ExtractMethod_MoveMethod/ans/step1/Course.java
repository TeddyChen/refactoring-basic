/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.FeatureEnvy.ExtractMethod_MoveMethod.ans.step1;

import java.util.LinkedList;
import java.util.List;

public class Course {
	private String _name;
	private Teacher _teacher;
	private List<Student> _students;
	
	public Course(String name){
		_name = name;
		_students = new LinkedList<>();
	}
	
	public String getName(){
		return _name;
	}
	
	public void setTeacher(Teacher arg){
		_teacher = arg;
	}
	
	public Teacher getTeacher(){
		return _teacher;
	}
	
	public void addStudent(Student arg){
		_students.add(arg);
	}
	
	public String report(){
		StringBuffer sb = new StringBuffer();
		
		sb.append("Course: ").append(_name).append("\n\n");
		
		sb.append("Teacher Information ====> ").append("\n");
		reportTeacher(sb);
		sb.append("\n");
		
		sb.append("Students Information ===> ").append("\n");
		for(Student each : _students){
			reportStudent(sb, each);
		}
		
		return sb.toString();
	}

	private void reportStudent(StringBuffer sb, Student each) {
		sb.append("Name: ").append(each.getName()).append("\n");
		sb.append("ID: ").append(each.getID()).append("\n");
		sb.append("Department: ").append(each.getDepartment()).append("\n");
	}

	private void reportTeacher(StringBuffer sb) {
		sb.append("Name: ").append(_teacher.getName()).append("\n");
		sb.append("Specialties: ").append("\n");
		for(String each : _teacher.getSpecialities()){
			sb.append(each).append("\n");
		}
	}
	
	
	
	
}


