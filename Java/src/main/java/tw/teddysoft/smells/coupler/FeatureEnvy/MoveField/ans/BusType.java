/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.FeatureEnvy.MoveField.ans;

public enum BusType {
	REGULAR, FREE, SPECIAL
}