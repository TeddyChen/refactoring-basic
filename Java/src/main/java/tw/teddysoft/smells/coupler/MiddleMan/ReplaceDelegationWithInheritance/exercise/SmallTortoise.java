/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.MiddleMan.ReplaceDelegationWithInheritance.exercise;

public class SmallTortoise {
	Hub _ap;
	
	public SmallTortoise(Hub ap){
		_ap = ap;
	}
	
	public int routePacket(Packet p){
		return _ap.routePacket(p);
	}

	public boolean wireConnect(){
		return _ap.wireConnect();
	}
}
