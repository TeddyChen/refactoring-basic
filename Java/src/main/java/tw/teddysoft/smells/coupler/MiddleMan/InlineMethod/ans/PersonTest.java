/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.MiddleMan.InlineMethod.ans;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonTest {

	@Test
	public void when_Person_has_no_middle_name_then_getFullName_does_not_show_middle_name() {
		Person teddy = new Person("Teddy", null, "Chen");
		assertEquals("Teddy Chen", teddy.getFullName());
	}

	@Test
	public void when_Person_has_middle_name_then_getFullName_shows_middle_name() {
		Person uncleBob = new Person("Robert", "Cecil", "Martin");
		assertEquals("Robert Cecil Martin", uncleBob.getFullName());
	}
}
