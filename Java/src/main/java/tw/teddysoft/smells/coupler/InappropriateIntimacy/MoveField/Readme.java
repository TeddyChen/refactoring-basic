/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.MoveField;

import tw.teddysoft.common.annotation.See;
import tw.teddysoft.smells.coupler.FeatureEnvy.MoveField.exercise.Bus;
import tw.teddysoft.smells.coupler.FeatureEnvy.MoveField.exercise.BusTest;
import tw.teddysoft.smells.coupler.FeatureEnvy.MoveField.exercise.BusType;
import tw.teddysoft.smells.coupler.FeatureEnvy.MoveField.exercise.Driver;

@See({Bus.class, BusTest.class, BusType.class, Driver.class})
public interface Readme {
}
