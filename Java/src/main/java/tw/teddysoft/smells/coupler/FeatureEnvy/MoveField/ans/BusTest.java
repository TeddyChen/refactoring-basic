/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.FeatureEnvy.MoveField.ans;

import static org.junit.Assert.*;

import org.junit.Test;

public class BusTest {

	@Test
	public void when_bus_is_regular_then_price_is_basic_and_service_interval_is_10_minutes() {
		Bus taipei307 = new Bus(15);
		Driver teddy = new Driver("Teddy", taipei307, BusType.REGULAR);
		taipei307.setDriver(teddy);
	
		assertEquals(15, taipei307.ticketPrice());
		assertEquals(10, taipei307.serivceIntervalInMinutes());
	}
	
	@Test
	public void when_bus_is_free_then_price_is_zero_and_service_interval_is_30_minutes() {
		Bus taipei307 = new Bus(15);
		Driver teddy = new Driver("Teddy", taipei307, BusType.FREE);
		taipei307.setDriver(teddy);
	
		assertEquals(0, taipei307.ticketPrice());
		assertEquals(30, taipei307.serivceIntervalInMinutes());
	}

	@Test
	public void when_bus_is_special_then_double_price_and_service_interval_is_5_minutes() {
		Bus taipei307 = new Bus(15);
		Driver teddy = new Driver("Teddy", taipei307, BusType.SPECIAL);
		taipei307.setDriver(teddy);
	
		assertEquals(30, taipei307.ticketPrice());
		assertEquals(5, taipei307.serivceIntervalInMinutes());
	}
}
