/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.MessageChains.HideDelegate.ans.step2;

public class Service {
	private String _name;
	private Host _host;
	
	public Service(String name){
		_name = name;
	}
	
	public void setHost(Host arg){
		_host = arg;
	}
	
	public String getName(){
		return _name;
	}
	
	public String getHostName(){
		return _host.getName();
	}

	public String getLocationContactName(){
		return _host.getLocation().getContact().getName();
	}
	
	// a lot of code here
}
