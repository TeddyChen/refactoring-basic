/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.MiddleMan.ReplaceDelegationWithInheritance.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class SmallTortoiseTest {

	@Test
	public void when_SmallTortoise_uses_a_middle_man_then_it_has_unnecessary_complexity() {
		SmallTortoise st = new SmallTortoise(new Hub());
		assertTrue(st.wireConnect());
	}
}
