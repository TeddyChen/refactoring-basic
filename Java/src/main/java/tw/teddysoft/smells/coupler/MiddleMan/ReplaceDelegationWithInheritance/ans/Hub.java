/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.MiddleMan.ReplaceDelegationWithInheritance.ans;

public class Hub {
	
	public Hub(){
	}
	
	public int routePacket(Packet p){
		// a lot of code
		return 0;
	}

	public boolean wireConnect(){
		// a lot of code
		return true;
	}
}
