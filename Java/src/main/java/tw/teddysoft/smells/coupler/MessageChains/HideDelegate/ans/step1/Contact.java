/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.MessageChains.HideDelegate.ans.step1;

public class Contact {
	private String _name;
	private String _phoneNumber;
	
	public Contact(String name, String phoneNumber){
		_name = name;
		_phoneNumber = phoneNumber;
	}
	
	public String getName(){
		return _name;
	}
	
	public void setPhoneNumber(String phoneNumber){
		_phoneNumber = phoneNumber;
	}
	
	public String getPhoneNumber(){
		return _phoneNumber;
	}
	
	// a lot of code here
}
