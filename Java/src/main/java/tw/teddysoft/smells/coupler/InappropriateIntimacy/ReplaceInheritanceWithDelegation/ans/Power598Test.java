/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.ReplaceInheritanceWithDelegation.ans;

import static org.junit.Assert.*;

import org.junit.Test;

public class Power598Test {

	@Test
	public void when_Power598_runs_under_monitoirng_then_it_runs_slowly() {
		Power598 engine = new Power598(new Engine307());
		engine.setUnderMonitoring(true);
		assertEquals(1000, engine.run());
	}

	@Test
	public void when_Power598_does_not_run_under_monitoirng_then_it_runs_normally() {
		Power598 engine = new Power598(new Engine307());
		engine.setUnderMonitoring(false);
		assertEquals(5000, engine.run());
	}
}
