/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.FeatureEnvy.MoveField.ans;

public class Bus {
	private Driver _driver;
	private int _ticketPrice;
	private BusType _busType;
	
	public Bus(int ticketPrice){
		_ticketPrice = ticketPrice;
	}

	public void setDriver(Driver arg){
		_driver = arg;
		_busType = _driver.getBusType();
	}
	
	public BusType getBusType(){
		return _busType;
	}
	
	public void setBusType(BusType arg){
		_busType = arg;
	}	
	
	public int ticketPrice(){
		int result =  _ticketPrice;
		if (_busType == BusType.SPECIAL){
			result = result * 2;
		}
		else if (_busType == BusType.FREE){
			result = 0;
		}
		return result;
	}

	public int serivceIntervalInMinutes(){
		int result = -1;
		if (_busType == BusType.SPECIAL){
			result = 5;
		}
		else if (_busType == BusType.FREE){
			result = 30;
		}
		else if (_busType == BusType.REGULAR){
			result = 10;
		}
		return result;
	}
}
