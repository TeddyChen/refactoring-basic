/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.FeatureEnvy.ExtractMethod_MoveMethod.exercise;

import tw.teddysoft.common.annotation.See;
import tw.teddysoft.smells.dispensable.DataClass.MoveMethod_EncapsulateField.exercise.Sensor;
import tw.teddysoft.smells.dispensable.DataClass.MoveMethod_EncapsulateField.exercise.Monitor;
import tw.teddysoft.smells.dispensable.DataClass.MoveMethod_EncapsulateField.exercise.ClassTest;

@See({Sensor.class, Monitor.class, ClassTest.class})
public interface Readme {
}
