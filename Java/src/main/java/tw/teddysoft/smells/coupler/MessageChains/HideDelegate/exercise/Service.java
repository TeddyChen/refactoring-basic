/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.MessageChains.HideDelegate.exercise;

public class Service {
	private String _name;
	private Host _host;
	
	public Service(String name){
		_name = name;
	}
	
	public void setHost(Host arg){
		_host = arg;
	}
	
	public Host getHost(){
		return _host;
	}
	
	public String getName(){
		return _name;
	}

	// a lot of code here
}
