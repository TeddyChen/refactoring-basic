/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.ReplaceInheritanceWithDelegation.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class Engine307Test {

	@Test
	public void when_Engine307_runs_under_monitoirng_then_it_runs_normally() {
		Engine307 engine = new Engine307();
		engine.setUnderMonitoring(true);
		assertEquals(5000, engine.run());
	}

	@Test
	public void when_Engine307_does_not_run_under_monitoirng_then_it_runs_normally() {
		Engine307 engine = new Engine307();
		engine.setUnderMonitoring(false);
		assertEquals(5000, engine.run());
	}
}
