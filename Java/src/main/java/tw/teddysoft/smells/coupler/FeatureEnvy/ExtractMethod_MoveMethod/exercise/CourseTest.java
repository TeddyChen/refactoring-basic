/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.FeatureEnvy.ExtractMethod_MoveMethod.exercise;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CourseTest {
	private Teacher _teddy;
	private Student _tony;
	private Student _john;
	private Student _mary;
	
	@Before
	public void setUp(){
		_teddy = new Teacher("Teddy Chen");
		_teddy.addSpeciality("Agile Methods and Practices");
		_teddy.addSpeciality("Exception Handling");
		_teddy.addSpeciality("Pattern and Pattern Language");
		
		_tony = new Student("Tony Cheng", "S098221", "CS");
		_john = new Student("John Wu", "S099510", "SE");
		_mary = new Student("Mary Lee", "S098212", "CS");
	}
	
	
	@Test
	public void when_a_course_is_set_correctly_then_a_course_report_is_generated_correctly() {
		String expected = "Course: Software Refactoring" + "\n\n" +
				"Teacher Information ====> " + "\n" +
				"Name: Teddy Chen" + "\n" +
				"Specialties: " + "\n" +
				"Agile Methods and Practices" + "\n" +
				"Exception Handling" + "\n" +
				"Pattern and Pattern Language" +  "\n\n" +
				"Students Information ===> " + "\n" +
				"Name: Tony Cheng" + "\n" +
				"ID: S098221" + "\n" +
				"Department: CS" + "\n" +
				"Name: John Wu" + "\n" +
				"ID: S099510" + "\n" +
				"Department: SE" + "\n" +
				"Name: Mary Lee" + "\n" +
				"ID: S098212"+  "\n" +
				"Department: CS" + "\n";
		
		Course refactoring = new Course("Software Refactoring");
		refactoring.setTeacher(_teddy);
		refactoring.addStudent(_tony);
		refactoring.addStudent(_john);
		refactoring.addStudent(_mary);
		
		assertEquals(expected, refactoring.report());
	}
}
