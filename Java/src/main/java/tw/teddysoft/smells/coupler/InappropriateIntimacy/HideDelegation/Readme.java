/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.coupler.InappropriateIntimacy.HideDelegation;

import tw.teddysoft.common.annotation.See;
import tw.teddysoft.smells.coupler.MessageChains.HideDelegate.exercise.Contact;
import tw.teddysoft.smells.coupler.MessageChains.HideDelegate.exercise.Host;
import tw.teddysoft.smells.coupler.MessageChains.HideDelegate.exercise.Location;
import tw.teddysoft.smells.coupler.MessageChains.HideDelegate.exercise.Service;
import tw.teddysoft.smells.coupler.MessageChains.HideDelegate.exercise.ServiceTest;

@See({Contact.class, Host.class, Location.class, Service.class, ServiceTest.class})
public interface Readme {
}
