package tw.teddysoft.smells.ooabuser.TemporaryField.ExtractClass.ans;

public class Person {
	private String _name;
	private String _id;
	private int _age;
	private int _sex; // 男 0  女 1
	
	public Person(String name, String id){
		_name = name;
		_id = id;
	}
	
	public String getName(){
		return _name;
	}
	
	public int getAge(){
		return _age;
	}
	
	public int getSex(){
		return _sex;
	}
	 //a log of code related to Person here

	
	public String createID(int location) {
		return new IDUtility().createID(location, _sex);
	}

	public boolean verifyID() {
		return new IDUtility().verifyID(_id);
	}
}
