/*
 * Adapted from Martin Fowler's Refactoring Book 
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.IntroduceNullObject.ans.step2;

public class NullCustomer extends Customer {

	public NullCustomer(String name) {
		super(name);
	}

	public PaymentHistory getHistory(){
		return PaymentHistory.newNull();
	}
	
	@Override
	public boolean isNull() {
		return true;
	}
}
