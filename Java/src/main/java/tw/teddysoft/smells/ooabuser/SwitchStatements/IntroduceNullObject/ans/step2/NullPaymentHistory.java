/*
 * Adapted from Martin Fowler's Refactoring Book 
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.IntroduceNullObject.ans.step2;

public class NullPaymentHistory extends PaymentHistory {

	public int getWeeksDelinquentInLastYear(){
		return 0;
	}

	@Override
	public boolean isNull() {
		return true;
	}
}
