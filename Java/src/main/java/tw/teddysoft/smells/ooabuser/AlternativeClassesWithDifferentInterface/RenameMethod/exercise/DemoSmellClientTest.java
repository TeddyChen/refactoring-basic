package tw.teddysoft.smells.ooabuser.AlternativeClassesWithDifferentInterface.RenameMethod.exercise;

import java.io.File;

import org.junit.Test;

public class DemoSmellClientTest {

	@Test
	public void demo_Log_Client() {
		Log.setLog(new File("./err.log"));
		Log.log(Log.INFO, "Applaction start.");
		Log.log(Log.WARN, "Memory low.");
		Log.log(Log.ERROR, "Cannot connection to database.");
		Log.log(Log.FATAL, "Server crash.");
	}
	
	@Test
	public void demo_Logger_Client() {
		Logger logger = Logger.makeLogger("./err.log");
		logger.informational("Applaction start.");
		logger.warning("Memory low.");
		// logger.error("Cannot connection to database.");
		logger.fatal("Server crash.");

		logger.informational("Applaction start.", new Exception());
		logger.warning("Memory low.", new Exception());
		// logger.error("Cannot connection to database.", new Exception());
		logger.fatal("Server crash.", new Exception());
	}
}
