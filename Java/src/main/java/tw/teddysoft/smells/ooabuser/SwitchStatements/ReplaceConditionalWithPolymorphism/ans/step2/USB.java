/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.ans.step2;

public class USB extends HardDriveType {
	@Override
	public int getTypeCode(){
		return USB;
	}
	
	@Override
	public boolean smartCheck(HardDrive hd){
		return hd.doUSBSmartCheck();
	}
}
