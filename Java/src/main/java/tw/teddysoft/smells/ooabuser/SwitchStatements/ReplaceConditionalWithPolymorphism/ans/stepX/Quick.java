/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.ans.stepX;

public class Quick implements RepairStrategy {

	@Override
	public int perform() {
		int repairedByte = 100;
		// a log of code
		return repairedByte;
	}

}
