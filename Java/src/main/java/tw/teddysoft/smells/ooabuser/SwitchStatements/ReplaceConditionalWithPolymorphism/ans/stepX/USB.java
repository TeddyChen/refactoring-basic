/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.ans.stepX;

public class USB extends HardDrive {

	public int getType(){
		return HardDrive.USB;
	}

	@Override
	public boolean smartCheck() {
		return doUSBSmartCheck();
	}
	
	private boolean doUSBSmartCheck() {
		// a lot of code
		return false;
	}
}
