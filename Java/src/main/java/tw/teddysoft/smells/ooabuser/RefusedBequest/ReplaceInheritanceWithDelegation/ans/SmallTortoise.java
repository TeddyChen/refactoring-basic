/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.RefusedBequest.ReplaceInheritanceWithDelegation.ans;

public class SmallTortoise {
	AccessPoint _ap;
	
	public SmallTortoise(AccessPoint ap){
		_ap = ap;
	}
	
	public int routePacket(Packet p){
		return _ap.routePacket(p);
	}

	public boolean wireConnect(){
		return _ap.wireConnect();
	}
}
