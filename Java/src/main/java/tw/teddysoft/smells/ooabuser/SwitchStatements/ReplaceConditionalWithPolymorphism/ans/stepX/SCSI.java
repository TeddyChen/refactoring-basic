/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.ans.stepX;

public class SCSI extends HardDrive {

	public int getType(){
		return HardDrive.SCSI;
	}

	@Override
	public boolean smartCheck() {
		return doSCSISmartCheck();
	}

	private boolean doSCSISmartCheck() {
		// a lot of code
		return true;
	}

}
