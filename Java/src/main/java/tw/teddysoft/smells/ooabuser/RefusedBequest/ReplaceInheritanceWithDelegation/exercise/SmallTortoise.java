/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.RefusedBequest.ReplaceInheritanceWithDelegation.exercise;

public class SmallTortoise extends AccessPoint{
	
	@Override
	public boolean wirelessConnect(){
		throw new RuntimeException("wireless connection is not supported by : " + this.getClass());
	}
	
	/*
	 * reduce visibility of the inherited method is not allowed
	@Override
	private boolean wirelessConnect(){
		// a lot of code
		return false;
	}
	*/
}
