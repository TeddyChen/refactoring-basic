/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.ans.step1;

public class USB extends HardDriveType {
	@Override
	public int getTypeCode(){
		return USB;
	}
}
