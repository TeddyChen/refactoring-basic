/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.exercise;

public class USB extends HardDriveType {
	@Override
	public int getTypeCode(){
		return USB;
	}
}
