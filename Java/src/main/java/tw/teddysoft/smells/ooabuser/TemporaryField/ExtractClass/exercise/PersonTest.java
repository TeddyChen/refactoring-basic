/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.TemporaryField.ExtractClass.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonTest {

	@Test
	public void when_providing_a_valid_ID_then_verify_success() {
		Person teddy = new Person("Teddy", "A124473257");
		assertEquals(true, teddy.verifyID());
	}

	@Test
	public void when_providing_a_invalid_ID_then_verify_fail() {
		Person teddy = new Person("Teddy", "B124473257");
		assertEquals(false, teddy.verifyID());
	}

}
