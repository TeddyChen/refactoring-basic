/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.ans.step2;

public class HardDrive {
	private HardDriveType _type;
	
	public HardDrive(int type){
		_type = HardDriveType.newType(type);
	}

	public int getType(){
		return _type.getTypeCode();
	}
	
	public void setType(int type){
		_type = HardDriveType.newType(type);
	}

	public boolean smartCheck(){
		return _type.smartCheck(this);
	}

	
	boolean doSATASmartCheck(){
		// a lot of code
		return true;
	}
	
	boolean doSASSmartCheck(){
		// a lot of code
		return true;
	}

	
	boolean doSCSISmartCheck(){
		// a lot of code
		return true;
	}

	
	boolean doUSBSmartCheck(){
		return false;
	}

}
