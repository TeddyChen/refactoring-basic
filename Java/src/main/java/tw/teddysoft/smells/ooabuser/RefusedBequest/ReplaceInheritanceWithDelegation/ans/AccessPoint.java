/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.RefusedBequest.ReplaceInheritanceWithDelegation.ans;

public class AccessPoint {
	
	public AccessPoint(){
	}
	
	public int routePacket(Packet p){
		// a lot of code
		return 0;
	}

	public boolean wireConnect(){
		// a lot of code
		return true;
	}
	
	public boolean wirelessConnect(){
		// a lot of code
		return true;
	}
}
