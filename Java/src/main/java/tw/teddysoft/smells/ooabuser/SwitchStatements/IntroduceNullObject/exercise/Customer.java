/*
 * Adapted from Martin Fowler's Refactoring Book 
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.IntroduceNullObject.exercise;

public class Customer {
	private String _name;
	private PaymentHistory history;
	
	public Customer(String name){
		_name = name;
	}
	
	public String getName(){
		return _name;
	}

	public void setHistory(PaymentHistory arg){
		history = arg;
	}
	
	public PaymentHistory getHistory(){
		return history;
	}
}
