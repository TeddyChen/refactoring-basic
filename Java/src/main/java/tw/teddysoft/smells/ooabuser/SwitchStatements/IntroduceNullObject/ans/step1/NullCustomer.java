/*
 * Adapted from Martin Fowler's Refactoring Book 
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.IntroduceNullObject.ans.step1;

public class NullCustomer extends Customer {

	public NullCustomer(String name) {
		super(name);
	}

	@Override
	public boolean isNull() {
		return true;
	}
}
