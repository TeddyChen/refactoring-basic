package tw.teddysoft.smells.ooabuser.AlternativeClassesWithDifferentInterface.RenameMethod.ans.step3;

import static org.junit.Assert.*;
import java.io.File;
import org.junit.Test;

import tw.teddysoft.smells.ooabuser.AlternativeClassesWithDifferentInterface.RenameMethod.ans.step1.Log;

public class DemoSmellClientTest {

	@Test
	public void demo_Log_Client() {
		Log.setLog(new File("./err.log"));
		Log.log(Log.INFO, "Applaction start.");
		Log.log(Log.WARN, "Memory low.");
		Log.log(Log.ERROR, "Cannot connection to database.");
		Log.log(Log.FATAL, "Server crash.");
		
		Log.log(Log.INFO, "Applaction start.", new Exception());
		Log.log(Log.WARN, "Memory low.", new Exception());
		Log.log(Log.ERROR, "Cannot connection to database.", new Exception());
		Log.log(Log.FATAL, "Server crash.", new Exception());
	}
	
	@Test
	public void demo_Logger_Client() {
		Log.setLog(new File("./err.log"));
		Log.log(Log.INFO, "Applaction start.");
		Log.log(Log.WARN, "Memory low.");
		Log.log(Log.ERROR, "Cannot connection to database.");
		Log.log(Log.FATAL, "Server crash.");		

		Log.log(Log.INFO, "Applaction start.", new Exception());
		Log.log(Log.WARN, "Memory low.", new Exception());
		Log.log(Log.ERROR, "Cannot connection to database.", new Exception());
		Log.log(Log.FATAL, "Server crash.", new Exception());
	}
}
