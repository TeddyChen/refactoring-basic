package tw.teddysoft.smells.ooabuser.TemporaryField.ExtractClass_IntroductNullObject;

import tw.teddysoft.common.annotation.See;
import tw.teddysoft.smells.bloater.LargeClass.ExtractClass.exercise.USB3Controller;
import tw.teddysoft.smells.bloater.LargeClass.ExtractClass.exercise.USBDrive;
import tw.teddysoft.smells.bloater.LargeClass.ExtractClass.exercise.USBDriveTest;

@See({USB3Controller.class, USBDrive.class, USBDriveTest.class})
public interface Readme {
}
