/*
 * Adapted from Martin Fowler's Refactoring Book 
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.IntroduceNullObject.ans.step2;

public class PaymentHistory implements Nullable{

	public static PaymentHistory newNull(){
		return new NullPaymentHistory();
	}
	
	public int getWeeksDelinquentInLastYear(){
		return 2;
	}

	@Override
	public boolean isNull() {
		return false;
	}
}
