/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceParameterWithExplicitMethods.ans;

import static org.junit.Assert.*;

import org.junit.Test;

public class HardDriveTest {
	
	@Test
	public void demo_using_explicit_method_to_create_different_types_of_objects(){
		HardDrive sata = HardDrive.createSATA();
		HardDrive sas =  HardDrive.createSAS();
		HardDrive scsi = HardDrive.createSCSI();
		HardDrive usb = HardDrive.createUSB();

		assertNotNull(sata);
		assertNotNull(sas);
		assertNotNull(scsi);
		assertNotNull(usb);
		
		assertTrue(sata instanceof SATA);
		assertTrue(sas instanceof SAS);
		assertTrue(scsi instanceof SCSI);
		assertTrue(usb instanceof USB);
	}
}
