package tw.teddysoft.smells.ooabuser.AlternativeClassesWithDifferentInterface.RenameMethod.ans.step2;

import static org.junit.Assert.*;
import java.io.File;
import org.junit.Test;

public class DemoSmellClientTest {

	@Test
	public void demo_Log_Client() {
		Log.setLog(new File("./err.log"));
		Log.log(Log.INFO, "Applaction start.");
		Log.log(Log.WARN, "Memory low.");
		Log.log(Log.ERROR, "Cannot connection to database.");
		Log.log(Log.FATAL, "Server crash.");
		
		Log.log(Log.INFO, "Applaction start.", new Exception());
		Log.log(Log.WARN, "Memory low.", new Exception());
		Log.log(Log.ERROR, "Cannot connection to database.", new Exception());
		Log.log(Log.FATAL, "Server crash.", new Exception());
	}
	
	@Test
	public void demo_Logger_Client() {
		Logger.setLog(new File("./err.log"));
		Logger.log(Log.INFO, "Applaction start.");
		Logger.log(Log.WARN, "Memory low.");
		Logger.log(Log.ERROR, "Cannot connection to database.");
		Logger.log(Log.FATAL, "Server crash.");		

		Logger.log(Log.INFO, "Applaction start.", new Exception());
		Logger.log(Log.WARN, "Memory low.", new Exception());
		Logger.log(Log.ERROR, "Cannot connection to database.", new Exception());
		Logger.log(Log.FATAL, "Server crash.", new Exception());
	}
}
