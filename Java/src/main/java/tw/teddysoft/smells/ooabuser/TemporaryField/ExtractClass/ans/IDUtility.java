package tw.teddysoft.smells.ooabuser.TemporaryField.ExtractClass.ans;

import java.util.Random;

public class IDUtility {

	private static final String _locations[][] = { { "A", "台北市" }, { "B", "台中市" }, { "C", "基隆市" }, { "D", "台南市" }, 
			{ "E", "高雄市" }, { "F", "台北縣" }, { "G", "宜蘭縣" }, { "H", "桃園縣" }, { "J", "新竹縣" }, { "K", "苗栗縣" }, 
			{ "L", "台中縣" }, { "M", "南投縣" }, { "N", "彰化縣" }, { "P", "雲林縣" }, { "Q", "嘉義縣" }, { "R", "台南縣" }, 
			{ "S", "高雄縣" }, { "T", "屏東縣" }, { "U", "花蓮縣" }, { "V", "台東縣" }, { "X", "澎湖縣" }, { "Y", "陽明山" },
			{ "W", "金門縣" }, { "Z", "連江縣" }, { "I", "嘉義市" }, { "O", "新竹市" } };

	public String createID(int location, int sex) {
		StringBuffer output = new StringBuffer();
		int[] number = new int[9];

		/*
		 * .....
		 * code that use _locations to generate ID
		 *  
		 */
		output.append(number[1] = sex + 1);
		Random r = new Random();
		for (int i = 2; i < number.length; i++) {
			output.append(number[i] = r.nextInt(10));
		}

		int sum = number[0];
		for (int i = number.length - 1, j = 1; i > 0; i--, j++) {
			sum += number[i] * j;
		}
		int check = (sum % 10) == 0 ? 0 : 10 - (sum % 10);
		output.append(check);
		return output.toString();
	}

	public boolean verifyID(String id) {

		int inte = -1;
		String s1 = String.valueOf(Character.toUpperCase(id.charAt(0)));
		for (int i = 0; i < 26; i++) {
			if (s1.compareTo(_locations[i][0]) == 0) {
				inte = i;
			}
		}
		int total = 0;
		int all[] = new int[11];
		String E = String.valueOf(inte + 10);
		int E1 = Integer.parseInt(String.valueOf(E.charAt(0)));
		int E2 = Integer.parseInt(String.valueOf(E.charAt(1)));
		all[0] = E1;
		all[1] = E2;
		try {
			for (int j = 2; j <= 10; j++)
				all[j] = Integer.parseInt(String.valueOf(id.charAt(j - 1)));
			for (int k = 1; k <= 9; k++)
				total += all[k] * (10 - k);
			total += all[0] + all[10];
			if (total % 10 == 0)
				return true;
		} catch (Exception ee) {
			ee.printStackTrace();
		}
		return false;
	}

}
