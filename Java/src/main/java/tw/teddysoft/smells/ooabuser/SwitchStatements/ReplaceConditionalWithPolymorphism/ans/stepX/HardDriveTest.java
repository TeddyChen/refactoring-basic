/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.ooabuser.SwitchStatements.ReplaceConditionalWithPolymorphism.ans.stepX;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class HardDriveTest {
	HardDrive _sata;
	HardDrive _sas;
	HardDrive _scsi; 
	HardDrive _usb;
	
	@Before
	public void setUp(){
		_sata = HardDrive.create(HardDrive.SATA);
		_sas =  HardDrive.create(HardDrive.SAS);
		_scsi = HardDrive.create(HardDrive.SCSI);
		_usb = HardDrive.create(HardDrive.USB);
	}
	
	@Test
	public void demo_repair_and_drive_type_are_independent() {
		RepairStrategy quick = new Quick();
		RepairStrategy saveMostDataFirst = new SaveMostDataFirst();
		RepairStrategy saveMostSpaceFirst = new SaveMostSpaceFirst();
		
		assertEquals(100, _sata.repair(quick));
		assertEquals(100, _sas.repair(quick));
		assertEquals(100, _scsi.repair(quick));
		assertEquals(100, _usb.repair(quick));
		
		assertEquals(500, _sata.repair(saveMostDataFirst));
		assertEquals(500, _sata.repair(saveMostDataFirst));
		assertEquals(500, _sata.repair(saveMostDataFirst));
		assertEquals(500, _sata.repair(saveMostDataFirst));	
		
		assertEquals(8000, _sata.repair(saveMostSpaceFirst));
		assertEquals(8000, _sas.repair(saveMostSpaceFirst));
		assertEquals(8000, _scsi.repair(saveMostSpaceFirst));
		assertEquals(8000, _usb.repair(saveMostSpaceFirst));
	}
	
	@Test
	public void demo_a_client_of_HardDrive() {
		assertEquals(HardDrive.SATA, _sata.getType());
		assertEquals(HardDrive.SAS, _sas.getType());
		assertEquals(HardDrive.SCSI, _scsi.getType());
		assertEquals(HardDrive.USB, _usb.getType());
		
		assertTrue(_sata.smartCheck());
		assertTrue(_sas.smartCheck());
		assertTrue(_scsi.smartCheck());
		assertFalse(_usb.smartCheck());
	}
}
