/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithClass.ans.step2;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonTest {

	@Test
	public void when_applying_ReplaceTypeCodeWithClass_setBloodGroup_will_not_out_of_range_anymore() {
		Person teddy = new Person(BloodGroup.O);
		assertEquals(BloodGroup.O, teddy.getBloodGroup());

		teddy.setBloodGroup(BloodGroup.AB);
		assertEquals(BloodGroup.AB, teddy.getBloodGroup());
	}
}
