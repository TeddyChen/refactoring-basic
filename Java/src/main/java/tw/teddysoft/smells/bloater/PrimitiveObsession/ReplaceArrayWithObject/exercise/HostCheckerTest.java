/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceArrayWithObject.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class HostCheckerTest {

	@Test
	public void when_call_execute_with_StubCriticalCommand_then_get_fake_result() {
		HostChecker checker = new HostChecker(new StubCriticalCommand());
		String [] result = checker.execute();
		assertEquals("Critical", result[0]);
		assertEquals("404", result[1]);
		assertEquals("Host not reachable", result[2]);
		assertEquals("30", result[3]);
		assertEquals("", result[4]);
	}
	
	
	private class StubCriticalCommand implements Command{
		@Override
		public String[] run() {
			String [] row = new String[5];
			row[0] = "Critical";
			row[1] = "404";
			row[2] = "Host not reachable";
			row[3] = "30";
			row[4] = "";
			return row;
		}
	}

}
