/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LongParameterList.IntroduceParameterObject.exercise;

import java.util.Hashtable;

public class Monitor {
	private Hashtable<String, Device> _devices = new Hashtable<>();
	
	public Monitor(){
	}
	
	public void addDevice(Device device){
		_devices.put(device.getName(), device);
	}

	public Hashtable<String, Device> getDevicesReadingBetween(double high, double low){
		Hashtable<String, Device> result = new Hashtable<>();
		
		for(Device device : _devices.values()){
			if ( (device.getReading() <= high) &&
				(device.getReading() >= low)){
				result.put(device.getName(), device);	
			}
		}
		return result;
	}
}
