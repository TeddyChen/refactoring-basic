/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractSubclass_ExtraceInterface.ans.step1;

public class USB3Drive extends USBDrive{
	private USB3Controller _controller;

	public USB3Drive(int version) {
		super(version);
		_controller = USB3Controller.newDefaultController();
	}
	
	public USB3Drive(int version, USB3Controller controller) {
		super(version);
		_controller = controller;
	}

	public int getSpeedInMegaBit(){
		if(null == _controller)
			throw new RuntimeException("Need a USB3Controller");
		return _controller.getChannelSpeed() * 1000;
	}
}
