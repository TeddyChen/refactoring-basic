/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithStateStrategy.ans.step2;

public class SATA extends HardDriveType {
	@Override
	public int getTypeCode(){
		return HardDrive.SATA;
	}
}
