/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithSubclasses.ans;

public class SATA extends HardDrive {
	
	public int getType(){
		return HardDrive.SATA;
	}

	@Override
	public boolean smartCheck() {
		return doSATASmartCheck();
	}

	private boolean doSATASmartCheck() {
		// a lot of code
		return true;
	}

}
