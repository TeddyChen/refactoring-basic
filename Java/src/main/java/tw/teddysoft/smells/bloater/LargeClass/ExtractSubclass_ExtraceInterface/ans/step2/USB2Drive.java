/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractSubclass_ExtraceInterface.ans.step2;

public class USB2Drive implements USBDrive {
	private static final int SPEED_MEGA_BIT = 480;
	
	public USB2Drive(){
	}
	
	@Override
	public int getSpeedInMegaBit(){
		return SPEED_MEGA_BIT;
	}
	
	@Override
	public boolean connect(){
		// a lot of code
		return true;
	}
	
	@Override
	public String checkDisk(){
		// a lot of code
		return "OK";
	}
}
