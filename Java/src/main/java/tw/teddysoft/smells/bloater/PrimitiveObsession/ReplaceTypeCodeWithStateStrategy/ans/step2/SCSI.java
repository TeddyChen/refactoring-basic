/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithStateStrategy.ans.step2;

public class SCSI extends HardDriveType {
	@Override
	public int getTypeCode(){
		return HardDrive.SCSI;
	}
}
