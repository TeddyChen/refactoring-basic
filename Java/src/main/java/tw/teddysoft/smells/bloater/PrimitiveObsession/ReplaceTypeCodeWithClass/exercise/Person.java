/*
 * Adapted from Martin Fowler's Refactoring Book
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithClass.exercise;

public class Person {
	
	public static final int O = 0;
	public static final int A = 1;
	public static final int B = 2;
	public static int AB = 3;
	
	private int _bloodGroup;
	
	public Person(int booldGroup){
		_bloodGroup = booldGroup;
	}
	
	public void setBloodGroup(int arg){
		_bloodGroup = arg;
	}
	
	public int getBooldGroup(){
		return _bloodGroup;
	}
	
}
