/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.DataClumps.ExtractClass_PreserveWholeObject.ans;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Student {
	
	// a lot of code
	
	public boolean save(ConnectionData connData){
		Connection conn = null;
		try
		{
		  Class.forName(connData.getDriverName()).newInstance();
		  conn = DriverManager.getConnection(connData.getURL(), connData.getUserName(), connData.getPassword());
		  // .. save this to database
		}
		catch (Exception e) {
			System.out.println(e);
			return false;
		}
		finally{
			try {
				if(null != conn)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}
