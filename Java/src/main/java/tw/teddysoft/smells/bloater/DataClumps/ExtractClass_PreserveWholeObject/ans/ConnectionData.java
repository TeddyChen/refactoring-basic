/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.DataClumps.ExtractClass_PreserveWholeObject.ans;

public class ConnectionData {
	public final String _driverName;
	public final String _URL;
	public final String _userName;
	public final String _password;

	public ConnectionData(String driverName,  String URL, String userName, String password) {
		_driverName = driverName;
		_URL = URL;
		_userName = userName;
		_password = password;
		
	}

	public String getDriverName(){
		return _driverName;
	}
	
	public String getURL(){
		return _URL;
	}
	
	public String getUserName(){
		return _userName;
	}
	
	public String getPassword(){
		return _password;
	}
	
}