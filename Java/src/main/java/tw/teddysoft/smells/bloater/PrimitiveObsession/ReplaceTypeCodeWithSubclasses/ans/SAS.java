/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithSubclasses.ans;

public class SAS extends HardDrive {

	public int getType(){
		return HardDrive.SAS;
	}

	@Override
	public boolean smartCheck() {
		return doSASSmartCheck();
	}

	private boolean doSASSmartCheck() {
		return true;
	}
}
