/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithStateStrategy.ans.step3;

public class HardDrive {
	static final int SATA = 0;
	static final int SAS = 1;
	static final int SCSI = 2;
	static final int USB = 3;
	private HardDriveType _type;
	
	public HardDrive(int type){
		setType(type);
	}

	public int getType(){
		return _type.getTypeCode();
	}
	
	public void setType(int type){
		switch(type){
		case SATA:
			_type = new SATA();
			break;
		case SAS:
			_type = new SAS();
			break;
		case SCSI:
			_type = new SCSI();
			break;	
		case USB:
			_type = new USB();
			break;	
		default:
			throw new IllegalArgumentException("Incorrent hard dirve type code");
		}
	}

	public boolean smartCheck(){
		boolean result = false;
		
		switch(getType()){
		case SATA:
			result = doSATASmartCheck();
			break;
		case SAS:
			result = doSASSmartCheck();
			break;
		case SCSI:
			result = doSCSISmartCheck();
			break;
		case USB:
			result = doUSBSmartCheck();
			break;
		default:
			throw new RuntimeException("Unsupported hard drive type: " + _type);
		}
		return result;
	}

	
	private boolean doSATASmartCheck(){
		// a lot of code
		return true;
	}
	
	private boolean doSASSmartCheck(){
		// a lot of code
		return true;
	}

	
	private boolean doSCSISmartCheck(){
		// a lot of code
		return true;
	}

	
	private boolean doUSBSmartCheck(){
		return false;
	}

}
