/*
 * Adapted from Martin Fowler's Refactoring Book 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceDataValueWithObject.exercise;

public class Order {
	private int _id;
	private String _customer;
	
	public Order(int id, String customer){
		_id = id;
		_customer = customer;
	}
	
	public int getID(){
		return _id;
	}
	
	public String getCustomer(){
		return _customer;
	}

	public void setCustomer(String customer){
		_customer = customer;
	}
}
