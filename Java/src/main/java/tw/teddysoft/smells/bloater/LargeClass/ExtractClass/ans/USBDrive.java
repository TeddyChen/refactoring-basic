/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractClass.ans;

public class USBDrive {
	private USBController _controller;
	
	public USBDrive(USBController controller){
		if(null == controller) 
			_controller = USBController.newNull();
		else
			_controller = controller;
	}
	
	public int getSpeedInMegaBit(){
		return _controller.getChannelSpeed();
	}
	
	public boolean connect(){
		// a lot of code
		return true;
	}
	
	public String checkDisk(){
		// a lot of code
		return "OK";
	}
	
	public int getVersion(){
		return _controller.getVersion();
	}
}
