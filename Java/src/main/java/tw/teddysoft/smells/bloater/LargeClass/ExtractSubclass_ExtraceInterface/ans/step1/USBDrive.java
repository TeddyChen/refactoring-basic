/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractSubclass_ExtraceInterface.ans.step1;

public class USBDrive {
	private static final int SPEED_MEGA_BIT = 480;
	private int _version;
	
	public USBDrive(int version){
		_version = version;
	}
	
	public int getSpeedInMegaBit(){
		if (isUSB2()){
			return SPEED_MEGA_BIT;
		}
		else{
			return 0;
		}
	}
	
	private boolean isUSB2(){
		return (_version <= 20) ? true : false;
	}
}
