/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithSubclasses.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class HardDriveTest {

	@Test
	public void demo_a_client_of_HardDrive() {
		HardDrive sata = new HardDrive(HardDrive.SATA);
		HardDrive sas = new HardDrive(HardDrive.SAS);
		HardDrive scsi = new HardDrive(HardDrive.SCSI);
		HardDrive usb = new HardDrive(HardDrive.USB);
		
		assertEquals(HardDrive.SATA, sata.getType());
		assertEquals(HardDrive.SAS, sas.getType());
		assertEquals(HardDrive.SCSI, scsi.getType());
		assertEquals(HardDrive.USB, usb.getType());
		
		assertTrue(sata.smartCheck());
		assertTrue(sas.smartCheck());
		assertTrue(scsi.smartCheck());
		assertFalse(usb.smartCheck());
	}
}
