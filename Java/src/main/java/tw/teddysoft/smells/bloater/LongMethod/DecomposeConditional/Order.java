/*
 * Adapted from Martin Fowler's Refactoring Book
 * 
 */
package tw.teddysoft.smells.bloater.LongMethod.DecomposeConditional;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

public class Order {
	private Date SUMMER_START;
	private Date SUMMER_END;
	private double _winterRate = 1.1;
	private double _winterServiceCharge = 20;
	private double _summerRate = 0.9;
	
	public double charge(Date date, int quantity){
		double result;
		if (date.before(SUMMER_START) || date.after(SUMMER_END))
			result = quantity * _winterRate + _winterServiceCharge;
		else
			result = quantity * _summerRate;
		return result;
	}
	
	
	public Order(){
		try {
			SUMMER_START = DateFormat.getDateInstance().parse("2013/06/01");
		} catch (ParseException e) {
			// log the exception
		}
		try {
			SUMMER_END = DateFormat.getDateInstance().parse("2013/10/31");
		} catch (ParseException e) {
			// log the exception
		}
	}
	

	
	
	
public class OrderV2 {
	
	public double charge(Date date, int quantity){

		double result;
		
		if (notSummer(date))
			result = winterCharge(quantity);
		else
			result = summerCharge(quantity);
		
		return result;
	}
	
	
	private boolean notSummer(Date date){
		return (date.before(SUMMER_START) || date.after(SUMMER_END));
	}

	private double winterCharge(int quantity){
		return quantity * _winterRate + _winterServiceCharge;
	}
	
	private double summerCharge(int quantity){
		return quantity * _summerRate;
	}
}
}
