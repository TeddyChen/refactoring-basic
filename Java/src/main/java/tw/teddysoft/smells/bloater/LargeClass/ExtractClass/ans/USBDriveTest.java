/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractClass.ans;

import static org.junit.Assert.*;

import org.junit.Test;

public class USBDriveTest {

	@Test
	public void when_create_USBDrive_with_a_null_controller_then_get_USB2_speed_and_version() {
		USBDrive drive = new USBDrive(null);
		assertEquals(480, drive.getSpeedInMegaBit());
		assertEquals(20, drive.getVersion());
	}

	@Test
	public void when_create_USBDrive_with_a_USB3Controller_then_get_USB3_speed_and_version() {
		USBDrive drive = new USBDrive(new USB3Controller("Intel"));
		assertEquals(5000, drive.getSpeedInMegaBit());
		assertEquals(30, drive.getVersion());
	}
}
