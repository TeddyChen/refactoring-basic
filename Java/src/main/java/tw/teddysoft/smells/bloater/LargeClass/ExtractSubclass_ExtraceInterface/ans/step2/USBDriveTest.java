/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractSubclass_ExtraceInterface.ans.step2;

import static org.junit.Assert.*;

import org.junit.Test;

public class USBDriveTest {

	@Test
	public void when_create_USB2Drive_then_get_USB2_speed() {
		USBDrive drive = new USB2Drive();
		assertEquals(480, drive.getSpeedInMegaBit());
	}

	@Test
	public void when_create_USB3Drive_with_a_USB3Controller_then_get_USB3_speed() {
		USBDrive drive = new USB3Drive(new USB3Controller("Intel"));
		assertEquals(5000, drive.getSpeedInMegaBit());
	}

	@Test
	public void when_create_USB3Drive_without_a_USB3Controller_then_get_USB3_speed() {
		USBDrive drive = new USB3Drive();
		assertEquals(5000, drive.getSpeedInMegaBit());
	}

	@Test
	public void when_create_USB3Drive_with_a_null_USB3Controller_then_raise_AssertionError() {
		try{
			USBDrive drive = new USB3Drive(null);
			assertEquals(5000, drive.getSpeedInMegaBit());
			fail();
		}
		catch(AssertionError e){
			assertTrue(true);
		}
	}

}
