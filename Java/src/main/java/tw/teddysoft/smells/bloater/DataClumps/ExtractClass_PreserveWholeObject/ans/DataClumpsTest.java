/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.DataClumps.ExtractClass_PreserveWholeObject.ans;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DataClumpsTest {
	ConnectionData _connData;

	@Before 
	public void setUp(){
		_connData = new ConnectionData("org.apache.derby.jdbc.ClientDriver", "jdbc:derby:testdb;create=true", "teddy", "ao1xx");
	}
	
	@Test
	public void demo_a_client_of_Course() {
		Course course = new Course(_connData);
		assertTrue(course.save());
	}

	@Test
	public void demo_a_client_of_Teacher() {
		Teacher teacher = new Teacher(_connData);
		// a lot of code
		assertTrue(teacher.save());
	}
	
	@Test
	public void demo_a_client_of_Student() {
		Student student = new Student();
		// a lot of code
		assertTrue(student.save(_connData));
	}
}
