/*
 * Adapted from Martin Fowler's Refactoring Book
 * 
 */
package tw.teddysoft.smells.bloater.LongMethod.ReplaceMethodWithMethodObject.ans;

public class Account {
	private String _firstName;
	private String _lastName;

	public int gamma(int inputVal, int quantity, int yearToDate){
		return new Gamma(this, inputVal, quantity, yearToDate).compute();
	}

	public int delta(){
		return 2;
	}
}
