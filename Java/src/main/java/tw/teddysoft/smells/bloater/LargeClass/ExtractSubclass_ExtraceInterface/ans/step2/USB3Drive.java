/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractSubclass_ExtraceInterface.ans.step2;

import org.junit.Assert;

public class USB3Drive extends USB2Drive{
	private USB3Controller _controller;

	public USB3Drive() {
		super();
		_controller = USB3Controller.newDefaultController();
	}

	public USB3Drive(USB3Controller controller) {
		Assert.assertNotNull(controller);
		_controller = controller;
	}

	@Override
	public int getSpeedInMegaBit(){
		return _controller.getChannelSpeed() * 1000;
	}
}
