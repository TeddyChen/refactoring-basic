/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceArrayWithObject.exercise;

public interface Command {
	public String [] run();
}
