/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LongParameterList.IntroduceParameterObject.ans;

public class Range {
	private final double _high;
	private final double _low;
	
	public Range(double high, double low){
		_high = high;
		_low = low;
	}
	
	public double getHigh(){
		return _high;
	}

	public double getLow(){
		return _low;
	}
	
	public boolean isInclude(double reading){
		if ((reading <= _high) && (reading >= _low))
			return true;
		else return false;
	}
}
