/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LongParameterList.PreserveWholeObject.ans;

public class Cat {
	private String _name;
	
	//a lot of code .....
	
	public Cat(String name){
		_name = name;
	}
	
	public String getDetailInfo(Person master){
		StringBuilder sb = new StringBuilder();

		sb.append("Cat Name: ").append(_name).append("\n");
		sb.append("Master Name: ").append(master.getName()).append("\n");
		sb.append("Master Address: ").append(master.getAddress()).append("\n");
		sb.append("Master masterPhone: ").append(master.getPhone()).append("\n");
		
		return sb.toString();
	}
}
