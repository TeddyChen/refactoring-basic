/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithClass.ans.step1;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonTest {

	@Test
	public void when_setBloodGroup_out_of_range_then_raise_ArrayIndexOutOfBoundsException() {
		Person teddy = new Person(Person.O);
		assertEquals(0, teddy.getBooldGroup());

		try{
			teddy.setBloodGroup(5);
			fail();
		}
		catch(ArrayIndexOutOfBoundsException e){
			assertTrue(true);
		}
	}
}
