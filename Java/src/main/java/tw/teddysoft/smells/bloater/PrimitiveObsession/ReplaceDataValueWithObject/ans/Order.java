/*
 * Adapted from Martin Fowler's Refactoring Book
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceDataValueWithObject.ans;

public class Order {
	private int _id;
	private Customer _customer;
	
	public Order(int id, String customerName){
		_id = id;
		_customer = new Customer(customerName);
	}
	
	public int getID(){
		return _id;
	}
	
	public String getCustomerName(){
		return _customer.getName();
	}

	public void setCustomer(String customerName){
		_customer = new Customer(customerName);
	}
}
