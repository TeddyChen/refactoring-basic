/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractSubclass_ExtraceInterface.ans.step1;

import static org.junit.Assert.*;

import org.junit.Test;

public class USBDriveTest {

	@Test
	public void when_create_USBDrive_with_version_20_then_get_USB2_speed() {
		USBDrive drive = new USBDrive(20);
		assertEquals(480, drive.getSpeedInMegaBit());
	}

	@Test
	public void when_create_USB3Drive_with_version_30_and_a_USB3Controller_then_get_USB3_speed() {
		USB3Drive drive = new USB3Drive(30, new USB3Controller("Intel"));
		assertEquals(5000, drive.getSpeedInMegaBit());
	}

	@Test
	public void when_create_USB3Drive_with_version_30_and_a_null_USB3Controller_then_raise_RuntimeException() {
		USB3Drive drive = new USB3Drive(30, null);
		try{
			assertEquals(5000, drive.getSpeedInMegaBit());
			fail();
		}
		catch(RuntimeException e){
			assertTrue(true);
		}
	}

	@Test
	public void when_create_USB3Drive_with_version_30_and_without_a_USB3Controller_then_get_USB3_speed() {
		USB3Drive drive = new USB3Drive(30);
		assertEquals(5000, drive.getSpeedInMegaBit());
	}

}
