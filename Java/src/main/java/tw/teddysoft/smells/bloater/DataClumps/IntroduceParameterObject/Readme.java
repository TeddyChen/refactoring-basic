/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.DataClumps.IntroduceParameterObject;

import tw.teddysoft.common.annotation.See;
import tw.teddysoft.smells.bloater.LongParameterList.IntroduceParameterObject.exercise.Device;
import tw.teddysoft.smells.bloater.LongParameterList.IntroduceParameterObject.exercise.Monitor;

@See({Device.class, Monitor.class})
public interface Readme {
}
