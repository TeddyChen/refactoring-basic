/*
 * Adapted from Martin Fowler's Refactoring Book
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithClass.ans.step1;

public class Person {
	
	public static final int O = BloodGroup.O.getCode();
	public static final int A = BloodGroup.A.getCode();
	public static final int B = BloodGroup.B.getCode();
	public static int AB = BloodGroup.AB.getCode();
	
	private BloodGroup _bloodGroup;
	
	public Person(int bloodGroup){
		_bloodGroup = BloodGroup.code(bloodGroup);
	}
	
	public void setBloodGroup(int arg){
		_bloodGroup = BloodGroup.code(arg);
	}
	
	public int getBooldGroup(){
		return _bloodGroup.getCode();
	}
	
}
