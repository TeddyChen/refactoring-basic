/*
 * Adapted from Martin Fowler's Refactoring Book
 * 
 */
package tw.teddysoft.smells.bloater.LongMethod.ReplaceTempWithQuery.ans;

public class Product {
	private int _itemPrice;
	private int _quantity;

	public Product(int itemPrice, int quantity){
		_itemPrice = itemPrice;
		_quantity = quantity;
	}
	
	public double getPrice(){
		return basePrice() * discountFactor();
	}
	
	private int basePrice(){
		return _quantity * _itemPrice;
	}
	
	public double discountFactor(){
		if(basePrice() > 1000) return  0.95;
		else return 0.98;
	}
}
