/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceDataValueWithObject.exercise;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class OrderTest {

	@Test
	public void demo_a_possible_client_of_Order_class() {
		List<Order> orders = new LinkedList<>();
		orders.add(new Order(1, "Teddy"));
		orders.add(new Order(2, "Kay"));
		orders.add(new Order(3, "Teddy"));
		orders.add(new Order(4, "Teddy"));
		orders.add(new Order(1, "Tony"));
		
		assertEquals(3, numberOfOrderFor(orders, "Teddy"));
	}
	
	private int numberOfOrderFor(List<Order> orders, String customer){
		int result = 0;
		for(Order each : orders){
			if (each.getCustomer().equals(customer))
			result++;
		}
		return result;
	}
	
}
