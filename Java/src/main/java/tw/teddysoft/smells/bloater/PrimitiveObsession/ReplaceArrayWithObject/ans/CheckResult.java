/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceArrayWithObject.ans;

public class CheckResult {
	private final Status _status;
	private final int _code;
	private final String _message;
	private final int _processingDuration;
	private final String _extraInfo;

	public CheckResult(Status status, int code, String message, int duration, String extraInfo){
		_status = status;
		_code = code;
		_message = message;
		_processingDuration = duration;
		_extraInfo = extraInfo;
	}
	
	public Status getStatus(){
		return _status;
	}
	
	public int getCode(){
		return _code;
	}
	
	public String getMessage(){
		return _message;
	}
	
	public int getProcessingDurationInSecond(){
		return _processingDuration;
	}
	
	public String getExtraInfo(){
		return _extraInfo;
	}
}
