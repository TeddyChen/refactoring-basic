/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithSubclasses.ans;

public class SCSI extends HardDrive {

	public int getType(){
		return HardDrive.SCSI;
	}

	@Override
	public boolean smartCheck() {
		return doSCSISmartCheck();
	}

	private boolean doSCSISmartCheck() {
		// a lot of code
		return true;
	}

}
