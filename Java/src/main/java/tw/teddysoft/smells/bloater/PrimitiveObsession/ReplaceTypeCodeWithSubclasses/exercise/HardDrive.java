/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithSubclasses.exercise;

public class HardDrive {

	static final int SATA = 0;
	static final int SAS = 1;
	static final int SCSI = 2;
	static final int USB = 3;
	
	private int _type;
	
	public HardDrive(int type){
		_type = type;
	}

	public int getType(){
		return _type;
	}
	
	public void setType(int type){
		type = _type;
	}

	public boolean smartCheck(){
		boolean result = false;
		
		switch(_type){
		case SATA:
			result = doSATASmartCheck();
			break;
		case SAS:
			result = doSASSmartCheck();
			break;
		case SCSI:
			result = doSCSISmartCheck();
			break;
		case USB:
			result = doUSBSmartCheck();
			break;
		default:
			throw new RuntimeException("Unsupported hard drive type: " + _type);
		}
		return result;
	}

	
	private boolean doSATASmartCheck(){
		// a lot of code
		return true;
	}
	
	private boolean doSASSmartCheck(){
		// a lot of code
		return true;
	}

	
	private boolean doSCSISmartCheck(){
		// a lot of code
		return true;
	}

	
	private boolean doUSBSmartCheck(){
		return false;
	}

}
