/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceTypeCodeWithStateStrategy.ans.step4;

public class HardDrive {
	private HardDriveType _type;
	
	public HardDrive(int type){
		_type = HardDriveType.newType(type);
	}

	public int getType(){
		return _type.getTypeCode();
	}
	
	public void setType(int type){
		_type = HardDriveType.newType(type);
	}

	public boolean smartCheck(){
		boolean result = false;
		
		switch(getType()){
		case HardDriveType.SATA:
			result = doSATASmartCheck();
			break;
		case HardDriveType.SAS:
			result = doSASSmartCheck();
			break;
		case HardDriveType.SCSI:
			result = doSCSISmartCheck();
			break;
		case HardDriveType.USB:
			result = doUSBSmartCheck();
			break;
		default:
			throw new RuntimeException("Unsupported hard drive type: " + _type);
		}
		return result;
	}

	
	private boolean doSATASmartCheck(){
		// a lot of code
		return true;
	}
	
	private boolean doSASSmartCheck(){
		// a lot of code
		return true;
	}

	
	private boolean doSCSISmartCheck(){
		// a lot of code
		return true;
	}

	
	private boolean doUSBSmartCheck(){
		return false;
	}

}
