/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ReplaceDataValueWithObject;

import tw.teddysoft.common.annotation.See;
import tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceDataValueWithObject.exercise.Order;

@See({Order.class,})
public interface Readme {
}
