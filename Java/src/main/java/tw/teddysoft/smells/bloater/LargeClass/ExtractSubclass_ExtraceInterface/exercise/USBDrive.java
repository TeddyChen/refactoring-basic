/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LargeClass.ExtractSubclass_ExtraceInterface.exercise;

public class USBDrive {
	private static final int SPEED_MEGA_BIT = 480;
	private int _version;
	private USB3Controller _controller;
	
	public USBDrive(int version, USB3Controller controller){
		_version = version;
		_controller = controller;
	}
	
	public int getSpeedInMegaBit(){
		if (isUSB2()){
			return SPEED_MEGA_BIT;
		}
		else {
			if(null == _controller)
				throw new RuntimeException("Need a USB3Controller");
			return _controller.getChannelSpeed() * 1000;
		}
	}
	
	private boolean isUSB2(){
		return (_version <= 20) ? true : false;
	}
	
	public boolean connect(){
		// a lot of code
		return true;
	}
	
	public String checkDisk(){
		// a lot of code
		return "OK";
	}
}
