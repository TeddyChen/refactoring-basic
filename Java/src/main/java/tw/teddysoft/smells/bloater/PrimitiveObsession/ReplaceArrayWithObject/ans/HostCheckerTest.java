/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.PrimitiveObsession.ReplaceArrayWithObject.ans;

import static org.junit.Assert.*;

import org.junit.Test;

public class HostCheckerTest {

	@Test
	public void when_call_execute_with_StubCriticalCommand_then_get_fake_result() {
		HostChecker checker = new HostChecker(new StubCriticalCommand());

		CheckResult result = checker.execute();
		assertEquals(Status.CRITICAL, result.getStatus());
		assertEquals(404, result.getCode());
		assertEquals("Host not reachable", result.getMessage());
		assertEquals(30, result.getProcessingDurationInSecond());
		assertEquals("", result.getExtraInfo());
	}
	
	private class StubCriticalCommand implements Command{
		@Override
		public CheckResult run() {
			return new CheckResult(Status.CRITICAL, 404, "Host not reachable", 30, "");
		}
	}
}
