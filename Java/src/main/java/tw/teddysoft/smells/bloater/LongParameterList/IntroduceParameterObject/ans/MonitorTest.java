/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.bloater.LongParameterList.IntroduceParameterObject.ans;

import static org.junit.Assert.*;

import java.util.Hashtable;

import org.junit.Test;

public class MonitorTest {

	@Test
	public void when_I_call_getDetailInfo_with_right_data_when_I_got_right_result() {
		
		Monitor monitor = new Monitor();
		monitor.addDevice(new Device("FAN1", 1500.0));
		monitor.addDevice(new Device("FAN2", 500.0));
		monitor.addDevice(new Device("FAN3", 0.0));
		monitor.addDevice(new Device("FAN4", 3000.0));
		
		Hashtable<String, Device> devices = monitor.getDevicesReadingBetween(new Range(5000, 1000));
		assertEquals(2, devices.size());
		assertNotNull(devices.get("FAN1"));
		assertNotNull(devices.get("FAN4"));
	}
}
