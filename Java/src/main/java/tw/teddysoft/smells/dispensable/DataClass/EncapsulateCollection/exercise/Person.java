/*
 * Adapted from Martin Fowler's Refactoring book
 */
package tw.teddysoft.smells.dispensable.DataClass.EncapsulateCollection.exercise;

import java.util.Set;

public class Person {
	private Set<Course> _courses;
	public Set<Course>  getCourses(){
		return _courses;
	}
	public void setCourses(Set<Course> arg){
		_courses = arg;
	}
}
