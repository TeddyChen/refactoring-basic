/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.SpeculativeGenerality.InlineClass;

import tw.teddysoft.common.annotation.See;
import tw.teddysoft.smells.dispensable.LazyClass.InlineClass.exercise.Person;
import tw.teddysoft.smells.dispensable.LazyClass.InlineClass.exercise.TelephoneNumber;

@See({Person.class, TelephoneNumber.class})
public interface Readme {
}
