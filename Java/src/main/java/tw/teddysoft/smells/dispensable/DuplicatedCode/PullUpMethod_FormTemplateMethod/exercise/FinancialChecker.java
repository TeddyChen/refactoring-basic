/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DuplicatedCode.PullUpMethod_FormTemplateMethod.exercise;

public abstract class FinancialChecker {
	public abstract boolean check(); 
}
