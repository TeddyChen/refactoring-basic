/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.Comments.ExtractMethod.exercise;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ThermalControllerTest {
	private boolean _isTernOn;
	
    @Before
    public void setUp() {
    	_isTernOn = true;
    }
    
	@Test
	public void when_device_temperature_is_in_normal_range_then_speedUp_success() {
		ThermalController controller = new ThermalController(55, _isTernOn);
		assertTrue(controller.speedUp());
	}

	@Test
	public void when_device_temperature_too_high_then_speedUp_fail() {
		ThermalController controller = new ThermalController(80, _isTernOn);
		assertFalse(controller.speedUp());
	}
	
	@Test
	public void when_device_does_not_turn_on_then_speedUp_fail() {
		_isTernOn = false;
		ThermalController controller = new ThermalController(55, _isTernOn);
		assertFalse(controller.speedUp());
	}
}
