/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.SpeculativeGenerality.exercise;

public class FileManipulator {
	AbstractFileCopier _copier;

	public FileManipulator(AbstractFileCopier copier){
		_copier = copier;
	}
	
	/*
	 * flag1 and flag2 are designed for Mac and Linux platform, respectively, 
	 * which is not used right now
	 */
	public boolean crossPlatformCopy(String source, String dest, boolean flag1, boolean flag2){
		return _copier.copy(source, dest);
	}
}
