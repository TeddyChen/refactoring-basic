/*
 * Adapted from Martin Fowler's Refactoring book
 */
package tw.teddysoft.smells.dispensable.DataClass.EncapsulateCollection.ans.step2;

public class Course {
	
	private String _name;
	private boolean _isAdvanced;
	
	public Course(String name, boolean isAdvanced){
		_name = name;
		_isAdvanced = isAdvanced;
	}
	
	public boolean isAdvanced(){
		return _isAdvanced;
	}
	
	public String getName(){
		return _name;
	}
}
