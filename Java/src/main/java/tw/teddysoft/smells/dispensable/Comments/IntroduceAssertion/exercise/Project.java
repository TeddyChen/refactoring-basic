/*
 * Adapted from Martin Fowler's Refactoring Book
 * 
 */
package tw.teddysoft.smells.dispensable.Comments.IntroduceAssertion.exercise;

public class Project {
	double _memberExpenseLimit;
	
	public Project(double memberExpenseLimit){
		_memberExpenseLimit = memberExpenseLimit;
	}

	public double getMemberExpenseLimit() {
		return _memberExpenseLimit;
	}
}
