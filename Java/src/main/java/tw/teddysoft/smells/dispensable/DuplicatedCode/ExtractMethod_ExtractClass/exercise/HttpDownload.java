/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DuplicatedCode.ExtractMethod_ExtractClass.exercise;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

public class HttpDownload {
	public static void download(String url, String localFileName) throws Exception
	{
		FileOutputStream fileOut = null;
		ByteArrayOutputStream byteOut = null;
		InputStream in = null;
		try {
			in = getInputStreamFromURL(url);
			fileOut = new FileOutputStream(localFileName);
			byteOut = new ByteArrayOutputStream();
			int read;
			while ((read = in.read()) != -1) {
				byteOut.write(read);
			}
			fileOut.write(byteOut.toByteArray());
		} catch(Exception e){
			FileUtil.delete(localFileName);
			throw e;
		} finally {
			try{
				if (null != byteOut)  byteOut.close();
			} catch(IOException e) { } // ignored 
			}
			try{
				if (null != fileOut) fileOut.close();
			} catch(IOException e) { }// ignored 
			try{
				if (null != in) in.close();
			}
			catch(IOException e) { }// ignored 
	}
	
	public static InputStream getInputStreamFromURL(String aAddress) throws MalformedURLException, IOException{
		URL url = new URL(aAddress);
		URLConnection conn = url.openConnection();
		conn.setConnectTimeout(0);
		conn.setReadTimeout(0);
		return conn.getInputStream();
	}
}
