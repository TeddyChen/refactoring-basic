/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.Comments.IntroduceAssertion.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmployeeTest {
	private final double PROJECT_MEMBER_EXENSE_LIMIT = 10000;
	private final double EMPLOYEE_EXENSE_LIMIE = 500;
	
	@Test
	public void when_employee_expense_limit_is_null_then_get_primary_project_member_expense_limit() {
		Employee teddy = new Employee(Employee.NULL_EXPENSE, new Project(PROJECT_MEMBER_EXENSE_LIMIT));
		assertEquals(PROJECT_MEMBER_EXENSE_LIMIT, teddy.getExpenseLimit(), 0.0000001);
	}

	@Test
	public void when_employee_expense_limit_is_not_null_then_get_employee_expense_limit() {
		Employee teddy = new Employee(EMPLOYEE_EXENSE_LIMIE, new Project(PROJECT_MEMBER_EXENSE_LIMIT));
		assertEquals(EMPLOYEE_EXENSE_LIMIE, teddy.getExpenseLimit(), 0.0000001);
	}
	
	@Test
	public void when_both_employee_expense_limit_and_primary_project_are_null_then_you_have_a_bug() {
		Employee teddy = new Employee(Employee.NULL_EXPENSE, null);
		
		try{
			assertEquals(PROJECT_MEMBER_EXENSE_LIMIT, teddy.getExpenseLimit(), 0.0000001);
			fail();
		}
		catch(NullPointerException e){
			assertTrue(true);
		}
	}

	
}
