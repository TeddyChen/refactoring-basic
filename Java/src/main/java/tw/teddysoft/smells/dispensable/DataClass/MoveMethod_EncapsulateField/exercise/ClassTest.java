/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DataClass.MoveMethod_EncapsulateField.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class ClassTest {

	@Test
	public void when_seneor_reading_is_50_then_monitor_is_critical() {
		Monitor monitor = new Monitor(new Sensor("CPU Speed", 50));
		assertTrue(monitor.isCritical());
	}

	@Test
	public void when_seneor_reading_is_49_then_monitor_is_not_critical() {
		Monitor monitor = new Monitor(new Sensor("CPU Speed", 49));
		assertFalse(monitor.isCritical());
	}

}
