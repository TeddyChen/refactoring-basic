/*
 * Adapted from Martin Fowler's Refactoring Book 
 * 
 */
package tw.teddysoft.smells.dispensable.Comments.IntroduceAssertion.exercise;

public class Employee {
	    public static final double NULL_EXPENSE = -1.0;
	    private double _expenseLimit = NULL_EXPENSE;
	    private Project _primaryProject;
	    
	    public Employee(double expenseLimit, Project primaryProject){
	    	_expenseLimit = expenseLimit;
	    	_primaryProject = primaryProject;
	    }

	    double getExpenseLimit() {
		   // should have either expense limit or a primary project
		   return (_expenseLimit != NULL_EXPENSE) ?
	           _expenseLimit:
	           _primaryProject.getMemberExpenseLimit();
	   }
	   
	   boolean withinLimit (double expenseAmount) {
	       return (expenseAmount <= getExpenseLimit());
	   }
}
	
