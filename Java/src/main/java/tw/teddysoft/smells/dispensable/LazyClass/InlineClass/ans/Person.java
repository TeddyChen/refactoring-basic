/*
 * Adapted from Martin Fowler's Refactoring Book
 */
package tw.teddysoft.smells.dispensable.LazyClass.InlineClass.ans;

public class Person {
	private String _name;
	private String _areaCode;
	private String _number;
	
	public String getName(){
		return _name;
	}
	
	public String getTelephoneNumber(){
		return ( "(" + _areaCode + ")" + _number);
	}
	
	public void setAreaCode(String areaCode) {
		_areaCode = areaCode;
	}
	
	public String getAreaCode(String areaCode) {
		 return _areaCode;
	}

	public String getNumber(){
		return _number;
	}

	public void setNumber(String number){
		_number = number;
	}
}
