/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DataClass.MoveMethod_EncapsulateField.exercise;

public class Monitor {
	private Sensor _sensor;

	public Monitor(Sensor sensor){
		_sensor = sensor;
	}

	public boolean isCritical() {
		if (_sensor._reading < Sensor.CriticalHighLimit)
			return false;
		else
			return true;
	}

	// a lot of code related to Monitor here
}
