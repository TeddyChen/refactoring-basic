/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DuplicatedCode.ExtractMethod_ExtractClass.ans.step2;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileUtil {
	
	public static boolean delete(String aFileName) throws IOException {
		File file = new File(aFileName);
		return delete(file);
	}

	public static boolean delete(File aResource) throws IOException {
		if (aResource.isDirectory()) {
			File[] childFiles = aResource.listFiles();

			for (int i = 0; i < childFiles.length; i++) {
				delete(childFiles[i]);
			}
		}
		return aResource.delete();
	}
}
