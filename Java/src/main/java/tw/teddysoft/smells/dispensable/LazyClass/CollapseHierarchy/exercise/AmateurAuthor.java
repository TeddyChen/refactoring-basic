/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.LazyClass.CollapseHierarchy.exercise;

public class AmateurAuthor extends Author{
	private String _primaryJobTitle;
	
	public AmateurAuthor(String firstName, String lastName) {
		super(firstName, lastName);
	}

	public void setPrimaryJabTitle(String primaryJobTitle){
		_primaryJobTitle = primaryJobTitle;
	}
	
	public String getPrimaryJobTitle(){
		return _primaryJobTitle;
	}
}
