/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DataClass.MoveMethod_EncapsulateField.ans;

public class Sensor {
	public static final double CriticalHighLimit = 50;
	private String _name;		// applying encapsulate field
	private double _reading;	// applying encapsulate field
	
	public Sensor(String name, double reading){
		_name = name;
		_reading = reading;
	}

	// applying move method
	public boolean isCritical() {
		if (_reading < Sensor.CriticalHighLimit)
			return false;
		else
			return true;
	}
	
	// applying encapsulate field
	public String getName(){
		return _name;
	}
	
	// applying encapsulate field
	public void setName(String name){
		_name = name;
	}

	// applying encapsulate field and remove setting method
	public double getReading(){
		_reading = getReadingFromHardWare();
		return _reading;
	}

	private double getReadingFromHardWare(){
		//Fake implementation
		return _reading;
	}
	
}
