/*
 * Adapted from Martin Fowler's Refactoring book
 */
package tw.teddysoft.smells.dispensable.DataClass.EncapsulateCollection.ans.step1;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;

public class Person {
	private Set<Course> _courses;
	
	public Person(){
		_courses = new HashSet<Course>();
	};
	
	public void addCourse(Course course){
		_courses.add(course);
	}
	
	public void removeCourse(Course course){
		_courses.remove(course);
	}
	
	public void setCourses(Set<Course> set){
		Assert.assertTrue(_courses.isEmpty());
		for(Course course : set){
			_courses.add(course);
		}
	}
	
	public Set<Course>  getCourses(){
		return Collections.unmodifiableSet(_courses);
	}
	
	public int numberOfCourses(){
		return _courses.size();
	}
	
	public int numberOfAdvancedCourse(){
		int count = 0;
		for(Course course : _courses){
			if (course.isAdvanced())
				count++;
		}
		return count;
	}
}
