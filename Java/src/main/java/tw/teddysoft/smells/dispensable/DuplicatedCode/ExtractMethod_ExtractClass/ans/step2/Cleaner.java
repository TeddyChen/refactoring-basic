/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DuplicatedCode.ExtractMethod_ExtractClass.ans.step2;

/*
 * Copyright (c) 2016 Teddysoft, Inc. All Rights Reserved.
 * 
 */

import java.io.Closeable;
import java.io.IOException;

public class Cleaner {
	public static void close(Closeable closeable) {
		if (null != closeable) {
			try {
				closeable.close();
			} catch (IOException e) {
				// logger.error("Fails to close a Closeable object, exception {} ", e.getMessage());
			}
		}
	}
	
	
}
