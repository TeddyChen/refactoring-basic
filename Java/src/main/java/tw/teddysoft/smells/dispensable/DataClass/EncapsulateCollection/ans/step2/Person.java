/*
 * Adapted from Martin Fowler's Refactoring book
 */
package tw.teddysoft.smells.dispensable.DataClass.EncapsulateCollection.ans.step2;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;

public class Person {
	private Set<Course> _courses;
	
	public Person(){
		_courses = new HashSet<Course>();
	};
	
	public void addCourse(Course course){
		_courses.add(course);
	}
	
	public void removeCourse(Course course){
		_courses.remove(course);
	}
	
	public void initializeCourses(Set<Course> set){
		Assert.assertTrue(_courses.isEmpty());
		_courses.addAll(set);
	}
	
	public Set<Course>  getCourses(){
		return Collections.unmodifiableSet(_courses);
	}
	
	public int numberOfCourses(){
		return _courses.size();
	}
	
	public int numberOfAdvancedCourse(){
		int count = 0;
		for(Course course : _courses){
			if (course.isAdvanced())
				count++;
		}
		return count;
	}
}
