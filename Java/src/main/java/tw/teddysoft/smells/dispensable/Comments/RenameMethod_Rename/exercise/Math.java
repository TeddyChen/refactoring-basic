/*
 * Adapted from Martin Fowler's Refactoring Book
 * 
 */
package tw.teddysoft.smells.dispensable.Comments.RenameMethod_Rename.exercise;

public class Math {

	// circular constant
	public final static double X = 3.1415926535897932384626433832795028841971;
	
	/*
	 * This is a special method for Teddy.
	 * It will return the sum of parameter a and b.
	 */
	public static int iLoveTeddy(int a, int b){
		return a+b;
	}
}
