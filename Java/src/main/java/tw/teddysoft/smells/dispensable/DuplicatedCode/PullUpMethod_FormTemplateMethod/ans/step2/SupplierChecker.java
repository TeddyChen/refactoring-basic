/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.DuplicatedCode.PullUpMethod_FormTemplateMethod.ans.step2;

public class SupplierChecker extends FinancialChecker{

	@Override
	protected boolean checkBank() {
		return (_bankBalance >= 10000000) ? true : false;
	}

	@Override
	protected boolean checkIncome() {
		return true;
	}


	public SupplierChecker(
			int bankBalance, 
			boolean creditStatus, 
			boolean loanStatus){
		
		super(bankBalance, creditStatus, loanStatus, 0, true);
	}
	
}
