/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.LazyClass.CollapseHierarchy.ans;

import static org.junit.Assert.*;

import org.junit.Test;

public class AuthorTest {

	@Test
	public void when_I_create_an_Author_object_then_I_can_call_its_setPrimaryJabTitle_methdod() {
		Author eiffel = new Author("She", "Eiffel");
		eiffel.setPrimaryJabTitle("Fat Cat");
	
		assertEquals("She", eiffel.getFirstName());
		assertEquals("Eiffel", eiffel.getLastName());
		assertEquals("Fat Cat", eiffel.getPrimaryJobTitle());
	}
}
