/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.SpeculativeGenerality.exercise;

public class FileManipulateFactory {

	public static AbstractFileCopier createFileCopier(){
		if (isWindows()){
			return new WindowsFileCopier();
		}
		else
		{
			// other platform will be supported 10 years later
			throw new RuntimeException("Platform is not supported.");
		}
	}
	
	
	private static boolean isWindows(){
		return true;
	}
	
}
