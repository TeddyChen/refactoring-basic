/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.dispensable.SpeculativeGenerality.exercise;

import static org.junit.Assert.*;

import org.junit.Test;

public class FileManipulateTest {

	@Test
	public void when_I_new_FileManipulate_with_FileManipulateFactory_I_got_a_partial_cross_platform_instacne_which_I_donot_need_right_now() {
		FileManipulator fm = new FileManipulator(FileManipulateFactory.createFileCopier());
		assertTrue(fm.crossPlatformCopy("./config.ini", "./bak/config.ini", false, false));
	}
}
