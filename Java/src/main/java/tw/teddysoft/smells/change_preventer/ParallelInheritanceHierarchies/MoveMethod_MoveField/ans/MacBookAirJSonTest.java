/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ParallelInheritanceHierarchies.MoveMethod_MoveField.ans;

import static org.junit.Assert.*;

import org.junit.Test;

public class MacBookAirJSonTest {

	@Test
	public void demo_the_Parallel_Inheritance_Hierarchies_Smell_was_removed() {
		Notebook mba = new MacBookAir();
		mba.setManufacturer("Apple");
		mba.setSerialNumber("SN09771223345");

		assertEquals("[{\"name\":\"Mac Book Air\",\"manufacturer\":\"Apple\",\"serialnumber\":\"SN09771223345\"}]", mba.toJson());
	}

}
