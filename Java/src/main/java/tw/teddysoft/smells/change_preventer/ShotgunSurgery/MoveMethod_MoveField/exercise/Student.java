/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ShotgunSurgery.MoveMethod_MoveField.exercise;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Student {
	
	// a lot of code
	
	public boolean save(String dbDriverName,  String dbURL, String dbUserName, String dbPassword){
		Connection conn = null;
		try
		{
		  Class.forName(dbDriverName).newInstance();
		  String url = dbURL;
		  conn = DriverManager.getConnection(url, dbUserName, dbPassword);
		  // .. save this to database
		}
		catch (Exception e) {
			System.out.println(e);
			return false;
		}
		finally{
			try {
				if(null != conn)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}
