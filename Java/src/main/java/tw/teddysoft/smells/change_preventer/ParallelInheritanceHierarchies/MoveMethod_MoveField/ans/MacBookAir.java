/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ParallelInheritanceHierarchies.MoveMethod_MoveField.ans;

public class MacBookAir extends Notebook {

	@Override
	String getName() {
		return "Mac Book Air";
	}

	@Override
	public String toJson() {
		StringBuilder sb = new StringBuilder();
		
		appendStart(sb);
		appendName(sb);
		appendSplitter(sb);
		appendManufacturer(sb);
		appendSplitter(sb);
		appendSerialNumber(sb);
		appendEnd(sb);
		
		return sb.toString();
	}
	
	private Notebook getNotebook(){
		return this;
	}
	
	private void appendStart(StringBuilder sb){
		sb.append("[{");
	}
	
	private void appendEnd(StringBuilder sb){
		sb.append("}]");
	}
	
	private void appendName(StringBuilder sb){
		sb.append("\"name\"").append(":").append("\"").append(this.getNotebook().getName()).append("\"");
	}

	private void appendSplitter(StringBuilder sb){
		sb.append(",");
	}
	
	private void appendManufacturer(StringBuilder sb){
		sb.append("\"manufacturer\"").append(":").append("\"").append(this.getNotebook().getManufacturer()).append("\"");
	}

	private void appendSerialNumber(StringBuilder sb){
		sb.append("\"serialnumber\"").append(":").append("\"").append(this.getNotebook().getSerialNumber()).append("\"");
	}
	
}
