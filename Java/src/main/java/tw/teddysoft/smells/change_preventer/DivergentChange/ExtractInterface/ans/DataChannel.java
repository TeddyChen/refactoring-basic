/*
 * Adapted from Robert C. Martin's Agile Software Development book
 */
package tw.teddysoft.smells.change_preventer.DivergentChange.ExtractInterface.ans;

public interface DataChannel {
	public void send(char c);
	public char recv();
}
