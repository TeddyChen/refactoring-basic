/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ParallelInheritanceHierarchies.MoveMethod_MoveField.exercise;

public abstract class NotebookJsonFormatter {
	private Notebook _notebook;
	
	NotebookJsonFormatter(Notebook notebook){
		_notebook = notebook;
	}

	protected Notebook getNotebook(){
		return _notebook;
	}
	
	public abstract String toJson();
}
