/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ParallelInheritanceHierarchies.MoveMethod_MoveField.exercise;

public class MacBookAir extends Notebook {

	@Override
	String getName() {
		return "Mac Book Air";
	}

}
