/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ShotgunSurgery.MoveMethod_MoveField.ans;

public class Teacher {
	Database _database;

	public Teacher(String dbDriverName, String dbURL, String dbUserName, String dbPassword){
		_database = new Database(dbDriverName, dbURL, dbUserName, dbPassword);
	}
	
	// a lot of code here
	
	public boolean save(){
		return _database.save(this);
	}
	
}
