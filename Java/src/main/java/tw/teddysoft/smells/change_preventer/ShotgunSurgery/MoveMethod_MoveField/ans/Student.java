/*
 * Copyright 2016 TeddySoft Technology. All rights reserved.
 * 
 */
package tw.teddysoft.smells.change_preventer.ShotgunSurgery.MoveMethod_MoveField.ans;

public class Student {
	
	public Student(){}
	
	// a lot of code
	
	public boolean save(String dbDriverName,  String dbURL, String dbUserName, String dbPassword){
		Database database = new Database(dbDriverName, dbURL, dbUserName, dbPassword);
		return database.save(this);
	}
}
